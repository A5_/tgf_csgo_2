#pragma once

#include <string>
#include <Windows.h>
#include <iostream>
#include <ctime>

#include "Singleton.h"

using namespace std;

#define console CConsole::Get()

class CConsole : public Singleton<CConsole>
{
public:

	void Setup( string Title = "" );
	void Free();

	template < typename ... args >
	void Log( args ... to_print )
	{
		time_t time_ = time( nullptr );
		string time_str = ctime( &time_ );
		time_str.pop_back();

		cout << time_str << ": ";

		print( to_print ... );
	}

	template <typename T >
	void print( T only )
	{
		cout << only << endl;
	}

	template <typename T, typename ... args >
	void print( T current, args... next )
	{
		cout << current << ' ';
		print( next... );
	}
};