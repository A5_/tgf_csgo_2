#include "CConsole.h"

void CConsole::Setup( string Title )
{
	AllocConsole();
	freopen( "CONIN$", "r", stdin );
	freopen( "CONOUT$", "w", stdout );
	freopen( "CONOUT$", "w", stderr );

	SetConsoleTitle( Title.c_str() );
}

void CConsole::Free()
{
	FreeConsole();
}