#pragma once

#include <windows.h>
#include <vector>
#include <string>

#include "Singleton.h"
#include "CConsole.h"

using namespace std;

#define modules CModules::Get()
#define getmodule( name ) modules.GetModule( name )->GetAddress()

class CModules : public Singleton<CModules>
{
public:
	HMODULE GetModule( string sModule );
	
public:
	HMODULE Client		= NULL;
	HMODULE Engine		= NULL;
	HMODULE VGui2		= NULL;
	HMODULE MatSurface	= NULL;
	HMODULE MatSystem	= NULL;
	HMODULE VPhysics	= NULL;
	HMODULE Studio		= NULL;
};