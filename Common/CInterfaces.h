#pragma once

#include <Windows.h>
#include <vector>
#include <string>

#include "Singleton.h"
#include "CSigScan.h"

#define intf CInterfaces::Get()
#define getinterface( type, module, name ) intf.GetInterface< type >( module, name )

using namespace std;

class IBaseClientDLL;
class IClientEntityList;
class ISurface;
class IVEngineClient;
class IVPanel;

class CInterfaces : public Singleton<CInterfaces>
{
public:
	void* GetInterface( HMODULE hModule, string sInterface );

public:

};

extern IBaseClientDLL* Client;
extern IClientEntityList* ClientEntList;
extern ISurface* Surface;
extern IVEngineClient* Engine;
extern IVPanel* Panel;