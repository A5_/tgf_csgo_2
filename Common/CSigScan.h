#pragma once

#include <windows.h>
#include <vector>
#include <string>

#include "Singleton.h"

#define signatures CSignatures::Get()
#define getsig( name ) signatures.GetSig( name )->GetAddress()

using namespace std;

typedef struct _SSig
{
	DWORD ulAddress;
	string sName;

	void SetAddress( DWORD ulNewAddress )
	{
		ulAddress = ulNewAddress;
	}

	DWORD GetAddress(){ return ulAddress; }

	string GetName(){ return sName; }

} SSig;

class CSignatures : public Singleton<CSignatures>
{
public:
	void			Clear();
	bool			SetRange( DWORD ulMin, DWORD ulMax );
	bool			SetRange( DWORD dwModule );
	SSig*			ManualAdd( string sName, DWORD ulAddressOf );
	SSig*			AddSig( string sName, string sPattern, DWORD offset = 0x0 );
	SSig*			GetSig( string sName );
	SSig*			GetSig( int iIndex );
	int				GetSigCount();
	vector< SSig >	GetVector();
	DWORD	        GetRangeMin();
	DWORD	        GetRangeMax();
	DWORD	        GetAddress( string sPattern );

private:
	DWORD	        ulRangeMin;
	DWORD	        ulRangeMax;
	vector< SSig >	vSigs;
};
