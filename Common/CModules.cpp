#include "CModules.h"

HMODULE CModules::GetModule( string sModule )
{
	if( sModule.empty() )
		return FALSE;

	HMODULE hModule = NULL;
	while( !hModule )
	{
		hModule = GetModuleHandleA( sModule.c_str() );
		if( !hModule )
			Sleep( 50 );
	}

#if _DEBUG
	console.Log( "[MODULES]", sModule, "->", hex, hModule );
#endif

	return hModule;
}