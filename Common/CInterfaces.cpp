#include "CInterfaces.h"

#include "CConsole.h"


void* CInterfaces::GetInterface( HMODULE hModule, string sInterface )
{
	typedef void* ( *CreateInterfaceFn )( const char* szName, int iReturn );
	CreateInterfaceFn CreateInterface = ( CreateInterfaceFn )GetProcAddress( hModule, "CreateInterface" );

	if( signatures.SetRange( hModule ) )
		char* cInterface = signatures.GetAddress( sInterface );
	else
		return NULL;

	void* pInterface = CreateInterface( cInterface, 0 );

#if _DEBUG
	console.Log( "[INTERFACE]", sInterface, "->", hex, pInterface );
#endif

	return pInterface;
}

IBaseClientDLL*		Client = NULL;
IClientEntityList*	ClientEntList = NULL;
ISurface*			Surface = NULL;
IVEngineClient*		Engine = NULL;
IVPanel*			Panel = NULL;
