#include "CSigScan.h"

#define INRANGE(x,a,b)		(x >= a && x <= b) 
#define GETBITS( x )		(INRANGE((x&(~0x20)),'A','F') ? ((x&(~0x20)) - 'A' + 0xa) : (INRANGE(x,'0','9') ? x - '0' : 0))
#define GETBYTE( x )		(GETBITS(x[0]) << 4 | GETBITS(x[1]))

void CSignatures::Clear()
{
	vSigs.clear();
}

bool CSignatures::SetRange( DWORD ulMin, DWORD ulMax )
{
	ulRangeMin = ulMin;
	ulRangeMax = ulMax;
	return true;
}

bool CSignatures::SetRange( DWORD dwModule )
{
	if( dwModule == NULL ) return false;

	PIMAGE_DOS_HEADER DosHead = ( IMAGE_DOS_HEADER* )dwModule;

	if( DosHead->e_magic != IMAGE_DOS_SIGNATURE )
		return false;

	PIMAGE_NT_HEADERS NTHead = ( PIMAGE_NT_HEADERS )( ( DWORD )DosHead + ( DWORD )DosHead->e_lfanew );

	if( NTHead->Signature != IMAGE_NT_SIGNATURE )
		return false;

	DWORD address = ( DWORD )dwModule + NTHead->OptionalHeader.BaseOfCode;
	DWORD size = address + NTHead->OptionalHeader.SizeOfCode;

	ulRangeMin = ( DWORD )address;
	ulRangeMax = ( DWORD )size;

	return true;
}

SSig* CSignatures::ManualAdd( string sName, DWORD ulAddressOf )
{
	SSig sig;

	sig.ulAddress = ulAddressOf;

	sig.sName = sName;

	vSigs.push_back( sig );

	return GetSig( this->vSigs.size() - 1 );
}

SSig* CSignatures::AddSig( string sName, string sPattern, DWORD offset )
{
	if( ulRangeMin == NULL || ulRangeMax == NULL )
		return FALSE;

	if( sName.empty() || sPattern.empty() )
		return FALSE;

	DWORD ulFound = GetAddress( sPattern );

	if( ulFound == 0 ) return 0;

	ulFound = *( DWORD* )( ulFound + offset );

	return ManualAdd( sName, ulFound );
}

SSig* CSignatures::GetSig( string sName )
{
	for( size_t i = 0; i < vSigs.size(); ++i )
	{
		if( vSigs[ i ].sName == sName )
			return &vSigs[ i ];
	}
	return NULL;
}

SSig* CSignatures::GetSig( int iIndex )
{
	return &vSigs[ iIndex ];
}

int CSignatures::GetSigCount()
{
	return GetVector().size();
}

vector< SSig > CSignatures::GetVector()
{
	return vSigs;
}


DWORD CSignatures::GetRangeMax()
{
	return ulRangeMax;
}


DWORD CSignatures::GetRangeMin()
{
	return ulRangeMin;
}

DWORD CSignatures::GetAddress( string sPattern )
{
	const char* pat = sPattern.c_str();
	DWORD firstMatch = 0;
	for( DWORD pCur = ulRangeMin; pCur < ulRangeMax; pCur++ )
	{
		if( !*pat ) return firstMatch;

		if( *( PBYTE )pat == '\?' || *( BYTE* )pCur == GETBYTE( pat ) )
		{
			if( !firstMatch ) firstMatch = pCur;
			if( !pat[ 2 ] ) return firstMatch;
			if( *( PWORD )pat == '\?\?' || *( PBYTE )pat != '\?' )
				pat += 3;
			else
				pat += 2; 
		}
		else{ pat = sPattern.c_str(); firstMatch = 0; }
	}

	return NULL;
}