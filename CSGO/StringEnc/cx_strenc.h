#pragma once
#include <string>
#include <array>
#include <cstdarg>
#include <codecvt>

#define BEGIN_NAMESPACE( x ) namespace x {
#define END_NAMESPACE }

BEGIN_NAMESPACE(XorCompileTime)
	constexpr auto time = __TIME__;
	constexpr auto seed = static_cast< int >( time[ 7 ] ) + static_cast< int >( time[ 6 ] ) * 10 + static_cast< int >( time[ 4 ] ) * 60 + static_cast< int >( time[ 3 ] ) * 600 + static_cast< int >( time[ 1 ] ) * 3600 + static_cast< int >( time[ 0 ] ) * 36000;

	template< int N >
	struct RandomGenerator
	{
	private:
		static constexpr unsigned a = 16807; 
		static constexpr unsigned m = 2147483647; 

		static constexpr unsigned s = RandomGenerator< N - 1 >::value;
		static constexpr unsigned lo = a * ( s & 0xFFFF ); 
		static constexpr unsigned hi = a * ( s >> 16 ); 
		static constexpr unsigned lo2 = lo + ( ( hi & 0x7FFF ) << 16 ); 
		static constexpr unsigned hi2 = hi >> 15; 
		static constexpr unsigned lo3 = lo2 + hi;

	public:
		static constexpr unsigned max = m;
		static constexpr unsigned value = lo3 > m ? lo3 - m : lo3;
	};

	template<>
	struct RandomGenerator< 0 >
	{
		static constexpr unsigned value = seed;
	};

	template< int N, int M >
	struct RandomInt
	{
		static constexpr auto value = RandomGenerator< N + 1 >::value % M;
	};

	template< int N >
	struct RandomChar
	{
		static const char value = static_cast< char >( 1 + RandomInt< N, 0x7F - 1 >::value );
	};

	template< size_t N, int K >
	struct XorString
	{
	private:
		const char _key;
		std::array< char, N + 1 > _encrypted;

		constexpr char enc( char c ) const { return c ^ _key; }

		char dec( char c ) const { return c ^ _key; }

	public:
		template< size_t... Is >
		constexpr __forceinline XorString( const char* str, std::index_sequence< Is... > )
			: _key( RandomChar< K >::value ),
			  _encrypted{ enc( str[ Is ] )... } { }

		__forceinline decltype(auto) decrypt( void )
		{
			for( size_t i = 0; i < N; ++i ) { _encrypted[ i ] = dec( _encrypted[ i ] ); }
			_encrypted[ N ] = '\0';
			return _encrypted.data();
		}
	};

	inline std::wstring s2ws( const std::string& str )
	{
		using convert_type = std::codecvt_utf8< wchar_t >;
		std::wstring_convert< convert_type, wchar_t > converter;

		return converter.from_bytes( str );
	}

#if _DEBUG
#define S_ENC( s ) ( s )
#define C_ENC( s ) ( s )
#define W_ENC( s ) ( s2ws( s ) )
#else
#define S_ENC( s ) std::string( XorCompileTime::XorString< sizeof( s ) - 1, __COUNTER__ >( s, std::make_index_sequence< sizeof( s ) - 1>() ).decrypt() )
#define C_ENC( s ) S_ENC( s ).c_str()
#define W_ENC( s ) s2ws(S_ENC(  s ) )
#endif

END_NAMESPACE
