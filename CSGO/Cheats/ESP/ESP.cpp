#include "ESP.h"

#include "../../HL/Entities/C_CSPlayer.h"
#include "../../HL/Entities/CBaseEntity.h"
#include "../../HL/SDK.h"

#include "Player.h"
#include "Entity.h"

ESP::ESP( std::shared_ptr< Draw > renderer_ ) { render = renderer_; }

ESP::~ESP() {}

void ESP::execute() const
{
	auto local = C_CSPlayer::getLocal();

	for( auto i = 0; i < i::ClientEntList->getHighestEntityIndex(); i++ )
	{
		auto entity =
				static_cast< CBaseEntity* >( i::ClientEntList->getClientEntity( i ) );

		if( !entity )
			continue;
		if( entity == reinterpret_cast< CBaseEntity* >( local ) )
			continue;

		if( entity->compareClientID( CCSPlayer ) ) { Player( render, entity ); } else { Entity( render, entity ); }
	}
}
