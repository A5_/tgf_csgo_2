#pragma once
#include "../../ImGui/Draw.h"
#include "../../Globals/Options.h"

class ESP
{
public:
	ESP( std::shared_ptr< Draw > renderer_ );
	~ESP();

	void execute() const;
private:

	std::shared_ptr< Draw > render;
};
