#include "Player.h"

Player::Player( std::shared_ptr< Draw > renderer_, hl::IClientEntity* player_ )
{
	render = renderer_;
	local = C_CSPlayer::getLocal();
	player = static_cast< C_CSPlayer* >( player_ );

	if( player->isDormant() || !player->isAlive() )
		return;

	if( !getBounding() )
		return;

	enemy = player->getTeamNum() != local->getTeamNum();

	height = bot - top;
	width = right - left;
	x_ = left;
	y_ = top;
	center = x_ + width / 2;

	offsetTop = -13.f;
	offsetBot = 3.f;

	execute();
}

Player::~Player() {}

void Player::execute()
{
	if( !option::get< bool >( esp_toggle ) )
		return;
	if( !option::get< bool >( esp_enemy ) && enemy )
		return;
	if( !option::get< bool >( esp_friendly ) && !enemy )
		return;
	if( option::get< bool >( esp_box ) )
		box();

	if( option::get< bool >( esp_name ) )
		name();
	if( option::get< bool >( esp_health ) )
		healthBar();
	if( option::get< bool >( esp_item ) )
		weapon();
}

void Player::box() const
{
	render->addRect( { x_, y_ }, width, height, enemy ?
		                                            hl::Color::Crimson() : hl::Color::RoyalBlue() );

	render->addRect( { x_ - 1, y_ - 1 }, width + 2, height + 2, hl::Color::Black() );
	render->addRect( { x_ + 1, y_ + 1 }, width - 2, height - 2, hl::Color::Black() );
}

void Player::name()
{
	render->addText( { center, top + offsetTop }, enemy ?
		                                              hl::Color::LightSalmon() : hl::Color::LightCyan(),
	                 outline | centered_x, player->getName().c_str() );

	offsetTop -= 15.f;
}

void Player::healthBar() const
{
	auto offset = width + 1.f;
	auto health = player->getHealth();
	auto hp = height - ( height * health / 100 );

	auto hue = health / 300.f;
	auto color = hl::Color::fromHSB( hue, 1.0f, 1.0f );

	render->addRect( { x_ + offset, top - 1.f },
	                 3.f, height + 2.f, hl::Color::Black() );

	render->addLine( { x_ + offset, bot - 1 },
	                 { x_ + offset, top + hp - 1 }, color );
}

void Player::weapon()
{
	render->addText( { center, bot + offsetBot },
	                 hl::Color::GoldenRod(), outline | centered_x,
	                 player->getActiveWeapon()->getName().c_str() );

	offsetBot += 15.f;
}

bool Player::getBounding()
{
	const auto trans = player->entityToWorldTransform();

	auto max = player->getCollideable()->obbMax();
	auto min = player->getCollideable()->obbMin();

	Vector points[] = { Vector( min.x, min.y, min.z ),
		Vector( min.x, max.y, min.z ),
		Vector( max.x, max.y, min.z ),
		Vector( max.x, min.y, min.z ),
		Vector( max.x, max.y, max.z ),
		Vector( min.x, max.y, max.z ),
		Vector( min.x, min.y, max.z ),
		Vector( max.x, min.y, max.z ) };

	Vector pointsTransformed[8];
	for( auto i = 0; i < 8; i++ ) { pointsTransformed[ i ] = points[ i ].transform( trans ); }

	Vector flb, brt, blb, frt, frb, brb, blt, flt;

	if( !i::Engine->worldToScreen( pointsTransformed[ 3 ], flb )
		|| !i::Engine->worldToScreen( pointsTransformed[ 5 ], brt )
		|| !i::Engine->worldToScreen( pointsTransformed[ 0 ], blb )
		|| !i::Engine->worldToScreen( pointsTransformed[ 4 ], frt )
		|| !i::Engine->worldToScreen( pointsTransformed[ 2 ], frb )
		|| !i::Engine->worldToScreen( pointsTransformed[ 1 ], brb )
		|| !i::Engine->worldToScreen( pointsTransformed[ 6 ], blt )
		|| !i::Engine->worldToScreen( pointsTransformed[ 7 ], flt ) )
		return false;

	Vector arr[] = { flb, brt, blb, frt, frb, brb, blt, flt };

	left = flb.x;
	top = flb.y;
	right = flb.x;
	bot = flb.y;

	for( auto& vec : arr )
	{
		if( left > vec.x )
			left = vec.x;
		if( bot < vec.y )
			bot = vec.y;
		if( right < vec.x )
			right = vec.x;
		if( top > vec.y )
			top = vec.y;
	}
	return true;
}
