#include "Entity.h"
#include "../../HL/Entities/C_BaseCombatWeapon.h"

Entity::Entity( std::shared_ptr< Draw > renderer_, hl::IClientEntity* entity_ )
{
	render = renderer_;
	entity = static_cast< CBaseEntity* >( entity_ );

	offsetTop = -13.f;
	offsetBot = 3.f;

	if( entity->isDormant() )
		return;

	auto pos3d = entity->getOrigin();

	if( !i::Engine->worldToScreen( pos3d, pos ) )
		return;

	execute();
}

Entity::~Entity() { }

void Entity::execute() const { if( entity->getOwner() == -1 ) { if( entity->isWeapon() ) { weaponName(); } } }

void Entity::weaponName() const
{
	auto weap = reinterpret_cast< C_BaseCombatWeapon* >( entity );

	render->addText( { pos.x, pos.y },
	                 hl::Color::LimeGreen(), outline,
	                 weap->getName().c_str() );
}
