#pragma once
#include "ESP.h"
#include "../../HL/Entities/c_csplayer.h"

class Player
{
public:
	Player( std::shared_ptr< Draw > renderer_, hl::IClientEntity* player_ );
	~Player();

	void execute();
private:
	void box() const;
	void name();
	void healthBar() const;
	void weapon();

	bool getBounding();

	std::shared_ptr< Draw > render;
	C_CSPlayer* player;
	C_CSPlayer* local;
	Vector pos, vtop, vbot;

	float width, height, center;
	float x_, y_;
	float offsetTop, offsetBot;
	float left, right, top, bot;

	bool enemy;
};
