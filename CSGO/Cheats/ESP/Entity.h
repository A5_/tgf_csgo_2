#pragma once
#include "ESP.h"
#include "../../HL/Entities/CBaseEntity.h"

class Entity
{
public:
	Entity( std::shared_ptr< Draw > renderer_, hl::IClientEntity* entity_ );
	~Entity();

	void execute() const;
private:
	void weaponName() const;

	std::shared_ptr< Draw > render;
	CBaseEntity* entity;

	Vector pos;

	float width, height;
	float x_, y_;
	float offsetTop, offsetBot;
	float left, right, top, bot;
};
