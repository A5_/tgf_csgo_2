#pragma once

namespace u
{
	namespace mem
	{
		inline uintptr_t getAbsolute(uintptr_t ptr, int offset, int size)
		{
			return ptr + *reinterpret_cast<uintptr_t*>(ptr + offset) + size;
		};
	}
}