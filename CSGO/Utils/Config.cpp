#include "Config.h"
#include "../Globals/Options.h"
#include <shlobj.h>
#include <fstream>

Config::Config()
{
	static TCHAR path[MAX_PATH];

	if( SUCCEEDED(SHGetFolderPathA(NULL, CSIDL_APPDATA, NULL, 0, path)) )
	{
		folder = std::string( path ) + S_ENC("\\Skype\\");
		file = std::string( path ) + S_ENC("\\Skype\\shared.bin");
	}

	CreateDirectoryA( folder.c_str(), nullptr );
}

void Config::startup()
{
	option::set( menu_visible, true );

	option::set( esp_toggle, true );
	option::set( esp_friendly, true );
	option::set( esp_enemy, true );
	option::set( esp_box, true );
	option::set( esp_name, true );
	option::set( esp_item, true );
	option::set( esp_health, true );
	option::set( esp_distance, 200.f );

	LOG("Default options loaded");
}

void Config::load() const
{
	std::ifstream in( file, std::ios::in | std::ios::binary );

	in.read(
		reinterpret_cast< char* >( &option::options ),
		sizeof uint32_t * Options::max );

	in.close();

	LOG("Config loaded");
}

void Config::save() const
{
	std::ofstream out( file, std::ios::out | std::ios::binary );

	out.write(
		reinterpret_cast< char* >( &option::options ),
		sizeof uint32_t * Options::max );

	out.close();

	LOG("Config saved");
}
