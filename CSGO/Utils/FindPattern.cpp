#include "FindPattern.h"
#include <Psapi.h>

uintptr_t u::mem::findPattern( uintptr_t module, std::string pattern )
{
	auto pat = pattern.c_str();
	uintptr_t firstMatch = 0;

	MODULEINFO miModInfo;
	GetModuleInformation(
		GetCurrentProcess(),
		reinterpret_cast< HMODULE >( module ),
		&miModInfo,
		sizeof(MODULEINFO)
	);

	uintptr_t module_end = module + miModInfo.SizeOfImage;

	for( auto current = module; current < module_end; current++ )
	{
		if( !*pat )
			return firstMatch;

		if( *( uint8_t* )pat == '\?' || *( uint8_t* )current == getByte(pat) )
		{
			if( !firstMatch )
				firstMatch = current;

			if( !pat[ 2 ] )
				return firstMatch;

			if( *( uint16_t* )pat == '\?\?' || *(uint8_t*)pat != '\?')
				pat += 3;
			else
				pat += 2; //one ?
		} 
		else
		{
			pat = pattern.c_str();
			firstMatch = 0;
		}
	}
	return NULL;
}
