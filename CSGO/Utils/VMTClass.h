#pragma once

#include <cstdint>
#include <windows.h>

class VMTClass
{
public:
	VMTClass()
	{
		classPtr_ = nullptr;
		newVmtPtr = nullptr;
		vmtPtr_ = nullptr;
		size_ = 0;
		old = 0;
	}

	virtual ~VMTClass() { destroy(); }

	bool isReady() const { return ( classPtr_ && newVmtPtr && vmtPtr_ ); }

	uint32_t getTableLength() const
	{
		uint32_t index = 0;

		for( ; vmtPtr_[ index ]; index++ )
		{
			if( IsBadCodePtr( reinterpret_cast< FARPROC >( vmtPtr_[ index ] ) ) )
				break;
		}
		return index;
	}

	void init( uintptr_t** classBase )
	{
		classPtr_ = classBase;
		vmtPtr_ = *classPtr_;
		size_ = getTableLength();

		if( size_ <= 0 )
			return;

		newVmtPtr = new uintptr_t[size_];
		memcpy( newVmtPtr, vmtPtr_, sizeof(uintptr_t) * size_ );

		VirtualProtect( classPtr_, sizeof(DWORD), PAGE_EXECUTE_READWRITE, &old );
		*classPtr_ = newVmtPtr;
		VirtualProtect( classPtr_, sizeof(DWORD), old, &old );
	}

	void* get() const { return reinterpret_cast< void* >( vmtPtr_ ); }

	template< typename T >
	T callMethod( uint32_t index )
	{
		if( index < 0 || index >= size_ )
			return 0;
		return reinterpret_cast< T >( vmtPtr_[ index ] );
	}

	uintptr_t* getVTable() const { return vmtPtr_; }

	template< typename T1, typename T2 >
	bool hookMethod( uint32_t index, T1 address, T2* out )
	{
		if( index < 0 || index >= size_ )
			return false;

		auto original = vmtPtr_[ index ];
		*out = reinterpret_cast< T2 >( original );

		newVmtPtr[ index ] = reinterpret_cast< uintptr_t >( address );

		return true;
	}

	void destroy()
	{
		restoreTable();
		if( newVmtPtr )
			delete[] newVmtPtr;
	}

	void restoreTable()
	{
		VirtualProtect( classPtr_, sizeof(DWORD), PAGE_EXECUTE_READWRITE, &old );
		*classPtr_ = vmtPtr_;
		VirtualProtect( classPtr_, sizeof(DWORD), old, &old );
	}

	bool operator !() const { return !isReady(); }

	void operator =( uintptr_t** classBase ) { init( classBase ); }

private:
	uintptr_t** classPtr_;
	uintptr_t* newVmtPtr;
	uintptr_t* vmtPtr_;
	uint32_t size_;
	DWORD old;
};

namespace hl
{
	template< typename T >
	T callMethod( void* vTable, int index )
	{
		if( vTable == nullptr )
			return 0;
		return ( *reinterpret_cast< T** >( vTable ) )[ index ];
	}
}
