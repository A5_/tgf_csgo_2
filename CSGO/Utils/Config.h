#pragma once
#include "../Include.h"

class Config
{
public:
	static Config& get()
	{
		static Config instance;
		return instance;
	}

	Config();

	static void startup();
	void load() const;
	void save() const;

private:
	std::string folder, file;
};
