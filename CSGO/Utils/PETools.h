#pragma once

#include "..\Include.h"
#include "PEB.h"

namespace u
{
	class pe
	{
	public:
		static PPEB getPEB();
		static FARPROC getProcAddress( HMODULE module, std::string name );
		static HMODULE getModuleHandle( std::string name );
		static bool getNTHeader( DWORD_PTR module, PIMAGE_NT_HEADERS* hdr );
	};
}
