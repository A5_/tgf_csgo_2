#include "Module.h"

#define MAKE_MODULE(s) {0,S_ENC(s)}

namespace m
{
	module_t modules[MODULE_MAX] =
	{
		MAKE_MODULE("client.dll"),
		MAKE_MODULE("engine.dll"),
		MAKE_MODULE("materialsystem.dll"),
		MAKE_MODULE("studiorender.dll"),
		MAKE_MODULE("vphysics.dll"),
		MAKE_MODULE("vstdlib.dll"),
		MAKE_MODULE("gameoverlayrenderer.dll"),
		MAKE_MODULE("shaderapidx9.dll")
	};
}
