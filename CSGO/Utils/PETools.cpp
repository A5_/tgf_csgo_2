#include "PETools.h"

PPEB u::pe::getPEB()
{
	__asm
	{
		mov eax, fs:[0x30]
	}
}

bool u::pe::getNTHeader( DWORD_PTR module, PIMAGE_NT_HEADERS* hdr )
{
	auto pDosHdr = reinterpret_cast< PIMAGE_DOS_HEADER >( module );

	if( !pDosHdr )
		return false;

	if( pDosHdr->e_magic != IMAGE_DOS_SIGNATURE )
		return false;

	auto pNTHdr =
			reinterpret_cast< PIMAGE_NT_HEADERS >( module + pDosHdr->e_lfanew );

	if( pNTHdr->Signature != IMAGE_NT_SIGNATURE )
		return false;

	if( hdr )
		*hdr = pNTHdr;

	return true;
}

inline std::string ws2s( const std::wstring& str )
{
	using convert_type = std::codecvt_utf8< wchar_t >;
	std::wstring_convert< convert_type, wchar_t > converter;

	return converter.to_bytes( str );
}

HMODULE u::pe::getModuleHandle( std::string name )
{
	auto pPEB = getPEB();

	if( pPEB != nullptr )
	{
		auto InLoadOrderModuleList = &pPEB->LoaderData->InLoadOrderModuleList;

		for( auto entry = InLoadOrderModuleList->Flink; entry != InLoadOrderModuleList; entry = entry->Flink )
		{
			auto module = CONTAINING_RECORD(entry, LDR_MODULE, InLoadOrderModuleList);

			auto name_s = ws2s( std::wstring( module->BaseDllName.Buffer, module->BaseDllName.Length ) );

			if( name.find( name_s.c_str() ) == 0 )
			{
				LOG(name,"found ->", std::hex, module->BaseAddress);
				return static_cast< HMODULE >( module->BaseAddress );
			}
		}
	}
	return nullptr;
}

FARPROC u::pe::getProcAddress( HMODULE module, std::string name )
{
	auto modulebase = reinterpret_cast< uint32_t >( module );

	PIMAGE_NT_HEADERS nthdr;

	if( !getNTHeader( modulebase, &nthdr ) )
		return nullptr;

	auto opthdr = &nthdr->OptionalHeader;
	auto datadir = &opthdr->DataDirectory[ IMAGE_DIRECTORY_ENTRY_EXPORT ];
	auto exportdir =
			reinterpret_cast< PIMAGE_EXPORT_DIRECTORY >( modulebase + datadir->VirtualAddress );

	auto ordtable = 
		reinterpret_cast< uint16_t* >( modulebase + 
			static_cast< uint32_t >( exportdir->AddressOfNameOrdinals ) );

	auto addrtable =
			reinterpret_cast< uint32_t* >( modulebase +
				static_cast< uint32_t >( exportdir->AddressOfFunctions ) );

	auto nametable =
			reinterpret_cast< uint32_t* >( modulebase +
				static_cast< uint32_t >( exportdir->AddressOfNames ) );

	for( uint16_t i = 0; i < exportdir->NumberOfNames; i++ )
	{
		auto procname = reinterpret_cast< char * >( modulebase + nametable[ i ] );

		if( !procname )
			continue;

		if( name.find( procname ) == 0 )
		{
			return reinterpret_cast< FARPROC >( modulebase +
				addrtable[ static_cast< uint16_t >( modulebase + ordtable[ i ] ) ] );
		}
	}
	return nullptr;
}
