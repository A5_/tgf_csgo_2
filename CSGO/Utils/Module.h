#pragma once
#include "..\Include.h"

enum eModules
{
	MODULE_CLIENT,
	MODULE_ENGINE,
	MODULE_MATERIAL,
	MODULE_STUDIO,
	MODULE_PHYSICS,
	MODULE_VSTDLIB,
	MODULE_OVERLAY,
	MODULE_DX9,

	MODULE_MAX
};

namespace m
{
	typedef struct
	{
		uintptr_t value;
		std::string name;
	} module_t;

	extern module_t modules[MODULE_MAX];

	inline uintptr_t getValue( eModules index ) { return modules[ index ].value; }
}
