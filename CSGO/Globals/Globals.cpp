#include "Globals.h"
#include "../HL/SDK.h"

namespace g
{
	HMODULE dll = nullptr;
	HWND window = nullptr;
	bool imguiInit = false;
	bool pressed[256] = {};
	bool held[256] = {};
}

namespace i
{
	VMTClass Client;
	VMTClass ClientMode;
	IClientEntityList* ClientEntList;
	ICVar* CVar;
	IEngine* Engine;
	VMTClass EngineTrace;
	VMTClass Globals;
	IModelRender ModelRender;
	IVModelInfo* ModelInfo;
	IMaterialSystem* MaterialSystem;
	VMTClass Material;
	IVRenderView* RenderView;
	VMTClass Prediction;
	VMTClass Physprops;
	VMTClass DebugOverlay;
	VMTClass StudioRender;
	VMTClass D3DDevice9;
}
