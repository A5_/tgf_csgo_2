#pragma once

enum Options
{
	menu_visible,

	/* esp */
	esp_toggle,
	esp_friendly,
	esp_enemy,
	esp_box,
	esp_name,
	esp_item,
	esp_health,
	esp_distance,

	/* chams */
	chams_toggle,
	chams_xqz,
	chams_mode,

	max
};
