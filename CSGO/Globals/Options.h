#pragma once

#include "Options_enum.h"
#include <cstdint>

namespace option
{
	uint32_t options[];

	template< typename T >
	T& get( Options o ) { return reinterpret_cast< T& >( options[ o ] ); }

	template< typename T >
	void set( Options o, T t ) { options[ o ] = uint32_t( t ); }
}
