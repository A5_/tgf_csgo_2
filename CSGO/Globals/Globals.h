#pragma once

#include "../Include.h"

namespace g
{
	extern HMODULE dll;
	extern HWND window;
	extern bool imguiInit;
	extern bool pressed[256];
	extern bool held[256];
}
