#include "Interface.h"
#include "../Utils/PETools.h"
#include "../HL/SDK.h"

#define GET_INTERFACE(_module,_name, _iface) if(!getInterface(\
m::getValue(_module), S_ENC(_name), &_iface)) return false

#define GET_INTERFACEP(_module,_name, _iface) if(!getInterfaceP(\
m::getValue(_module), S_ENC(_name), &_iface)) return false

bool Interface::startup()
{
	GET_INTERFACE(MODULE_CLIENT, "VClient0", i::Client);
	GET_INTERFACE(MODULE_ENGINE, "EngineTraceClient0", i::EngineTrace);
	GET_INTERFACE(MODULE_ENGINE, "VEngineModel0", i::ModelRender);
	GET_INTERFACE(MODULE_STUDIO, "VStudioRender0", i::StudioRender);
	GET_INTERFACEP(MODULE_MATERIAL, "VMaterialSystem0", i::MaterialSystem);
	GET_INTERFACEP(MODULE_ENGINE, "VEngineClient0", i::Engine);
	GET_INTERFACEP(MODULE_ENGINE, "VModelInfoClient0", i::ModelInfo);
	GET_INTERFACEP(MODULE_ENGINE, "VEngineRenderView0", i::RenderView);
	GET_INTERFACEP(MODULE_VSTDLIB, "VEngineCvar0", i::CVar);
	GET_INTERFACEP(MODULE_CLIENT, "VClientEntityList0", i::ClientEntList);

	auto clientmode = **reinterpret_cast< uintptr_t** >(
		i::Client.getVTable()[ 10 ] + 0x5 );
	LOG("ClientMode", std::hex, clientmode);
	i::ClientMode = reinterpret_cast< uintptr_t** >( clientmode );

	/*auto globals = u::mem::findPattern(
		m::getValue(MODULE_CLIENT), S_ENC("A1 ? ? ? ? 5F 8B 40 10")) + 1;
	LOG("Globals", std::hex, globals);
	i::Globals = reinterpret_cast<uintptr_t**>(globals);*/

	auto device = **reinterpret_cast< uintptr_t** >( u::mem::findPattern(
		m::getValue( MODULE_DX9 ), S_ENC("A1 ? ? ? ? 50 8B 08 FF 51 0C") ) + 1 );
	LOG("D3DDevice9", std::hex, device);
	i::D3DDevice9 = reinterpret_cast< uintptr_t** >( device );

	return true;
}

bool Interface::getInterfacePointer(
	uintptr_t module, std::string name, void** out )
{
	auto proc = u::pe::getProcAddress(
		reinterpret_cast< HMODULE >( module ),
		C_ENC("CreateInterface") );

	if( !proc )
		return false;

	auto interfaceList =
			getInterfaceList( reinterpret_cast< uintptr_t >( proc ) );

	if( !interfaceList )
		return false;

	while( interfaceList )
	{
		if( !std::string( interfaceList->name ).find( name ) )
		{
			*out = reinterpret_cast< void* >(
				static_cast< uintptr_t(*)() >( interfaceList->func )() );

			LOG(interfaceList->name, static_cast<uintptr_t**>(*out));

			return true;
		}
		interfaceList = interfaceList->next;
	}
	return false;
}

Interface::interface_t* Interface::getInterfaceList( uintptr_t iface )
{
	auto listPtr = iface - 0x6A;

	uintptr_t list;
	memcpy( &list, reinterpret_cast< void* >( listPtr ), 4 );

	return *reinterpret_cast< interface_t** >( list );
}
