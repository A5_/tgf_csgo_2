#include "../Include.h"

DWORD WINAPI DllMain( HMODULE hDll, DWORD dwReason, LPVOID lpReserved )
{
	switch( dwReason )
	{
		case DLL_PROCESS_ATTACH:
		{
			CreateThread( nullptr, NULL,
			              reinterpret_cast< LPTHREAD_START_ROUTINE >( startup ),
			              nullptr, NULL, nullptr );

			g::dll = hDll;
			return TRUE;
		}
		case DLL_PROCESS_DETACH:
			return TRUE;
		default:
			return FALSE;
	}
}
