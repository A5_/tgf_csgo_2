#pragma once

#include "../Include.h"

class Interface
{
	typedef struct interface_s
	{
		void* func;
		char* name;
		interface_s* next;
	} interface_t;

public:
	static Interface& get()
	{
		static Interface instance;
		return instance;
	}

	static bool startup();

private:
	static bool getInterfacePointer( uintptr_t module, std::string name, void** out );
	static interface_t* getInterfaceList( uintptr_t iface );

	static bool getInterface(
		uintptr_t module, std::string name, VMTClass* classPtr )
	{
		void* interface_ = nullptr;

		if( !getInterfacePointer( module, name, &interface_ ) )
			return false;

		*classPtr = static_cast< uintptr_t** >( interface_ );

		return true;
	}

	template< typename T >
	static bool getInterfaceP(
		uintptr_t module, std::string name, T* classPtr )
	{
		void* interface_ = nullptr;

		if( !getInterfacePointer( module, name, &interface_ ) )
			return false;

		*classPtr = static_cast< T >( interface_ );

		return true;
	}
};
