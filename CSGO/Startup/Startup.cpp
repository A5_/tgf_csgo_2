#include "../Include.h"

#include "Interface.h"
#include "../Utils/PETools.h"
#include "../Utils/Config.h"
#include "../HL/Netvar.h"
#include "../Hooks/Hooks.h"

bool findModules()
{
	LOG("Finding modules");

	auto failed = false;

	for( auto& module : m::modules )
	{
		module.value =
				reinterpret_cast< uintptr_t >( u::pe::getModuleHandle( module.name ) );

		if( !module.value )
			failed = true;
	}

	if( failed )
	{
		LOG("Could not find one or more required modules");
		return false;
	}

	return true;
}

bool startup()
{
	LOG_INIT;
	LOG("Startup");

	while( !( ( g::window = FindWindowA( C_ENC("Valve001"), nullptr ) ) ) )
		Sleep( 200 );

	if( !findModules() )
		return false;

	Config::get().startup();
	Config::get().load();

	if( !Interface::get().startup() )
	{
		LOG("Interface Failed");
		LOG_FREE;
		FreeLibraryAndExitThread( g::dll, 0 );
	}

	if( !Netvar::get().startup() )
	{
		LOG("Netvar Failed");
		LOG_FREE;
		FreeLibraryAndExitThread( g::dll, 0 );
	}

	if( !Hooks::get().startup() )
	{
		LOG("Hooks Failed");
		LOG_FREE;
		FreeLibraryAndExitThread( g::dll, 0 );
	}

	while( !g::pressed[ VK_DELETE ] ) { }

	Hooks::free();

	LOG_FREE;
	FreeLibraryAndExitThread( g::dll, 0 );
}
