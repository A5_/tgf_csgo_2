#pragma once

#include "StringEnc/cx_strenc.h"

#include <Windows.h>
#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <vector>
#include <ctime>
#include <sstream>

#include "Globals/Globals.h"
#include "Utils/VMTClass.h"
#include "Utils/Log.h"
#include "Utils/FindPattern.h"
#include "Utils/Memory.h"
#include "Utils/Module.h"


bool startup();