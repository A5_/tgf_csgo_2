#pragma once
#include "../Include.h"
#include "SDK.h"

#define GET_NETVAR(table_, prop_) static int netvar = Netvar::get().getOffset(table_, prop_)

class NetvarClass
{
public:
	template< class T >
	T getValue( void* ptr, uint32_t offset ) { return *reinterpret_cast< T* >( reinterpret_cast< uintptr_t >( ptr ) + offset ); }

	template< class T >
	T* getPointer( void* ptr, uint32_t offset ) { return reinterpret_cast< T* >( reinterpret_cast< uintptr_t >( ptr ) + offset ); }

	template< typename T >
	static T getMethod( void* classPtr, uint32_t index )
	{
		auto table = *static_cast< uintptr_t** >( classPtr );
		return reinterpret_cast< T >( table[ index ] );
	}
};

class Netvar
{
public:
	static Netvar& get();

	bool startup();
	int getOffset( std::string tableName, std::string propName );
	bool hookProp( std::string tableName, std::string propName, RecvVarProxyFn fun );

private:
	int getProp( const char* tableName, const char* propName, RecvProp** prop = nullptr );
	int getProp( RecvTable* recvTable, const char* propName, RecvProp** prop = nullptr ) const;
	RecvTable* getTable( const char* tableName );

private:
	std::vector< RecvTable* > m_tables;
};
