#include "Netvar.h"

Netvar& Netvar::get()
{
	static Netvar instance_;
	return instance_;
}

bool Netvar::startup()
{
	m_tables.clear();

	using getAllClassesFn = ClientClass*(__stdcall*)();
	auto clientClass = i::Client.callMethod< getAllClassesFn >( 8 )();

	if( !clientClass )
		return false;

	while( clientClass )
	{
		auto recvTable = clientClass->recvTable;
		m_tables.push_back( recvTable );

		clientClass = clientClass->next;
	}
	return true;
}

int Netvar::getOffset( std::string tableName, std::string propName )
{
	auto offset = getProp( tableName.c_str(), propName.c_str() );
	LOG(propName, std::hex, offset);
	if( !offset )
		return 0;
	return offset;
}

bool Netvar::hookProp( std::string tableName, std::string propName, RecvVarProxyFn fun )
{
	RecvProp* recvProp = nullptr;
	getProp( tableName.c_str(), propName.c_str(), &recvProp );
	if( !recvProp )
		return false;

	recvProp->proxyFn = fun;
	return true;
}

int Netvar::getProp( const char* tableName, const char* propName, RecvProp** prop )
{
	auto recvTable = getTable( tableName );
	if( !recvTable )
		return 0;

	auto offset = getProp( recvTable, propName, prop );
	if( !offset )
		return 0;

	return offset;
}

int Netvar::getProp( RecvTable* recvTable, const char* propName, RecvProp** prop ) const
{
	auto extraOffset = 0;
	for( auto i = 0; i < recvTable->numProps; ++i )
	{
		auto recvProp = &recvTable->prop[ i ];
		auto child = recvProp->dataTable;

		if( child && ( child->numProps > 0 ) )
		{
			auto tmp = getProp( child, propName, prop );
			if( tmp )
				extraOffset += ( recvProp->offset + tmp );
		}

		if( _stricmp( recvProp->varName, propName ) )
			continue;

		if( prop )
			*prop = recvProp;

		return ( recvProp->offset + extraOffset );
	}
	return extraOffset;
}

RecvTable* Netvar::getTable( const char* tableName )
{
	if( m_tables.empty() )
		return nullptr;

	for( auto table : m_tables )
	{
		if( !table )
			continue;
		if( _stricmp( table->tableName, tableName ) == 0 )
			return table;
	}
	return nullptr;
}
