#include "C_CSPlayer.h"
#include "../SDK.h"
#include "../Utl/CHandle.h"

C_CSPlayer* C_CSPlayer::getLocal()
{
	return reinterpret_cast<C_CSPlayer*>(
		i::ClientEntList->getClientEntity(i::Engine->getLocalPlayer()));
}

C_BaseCombatWeapon* C_CSPlayer::getActiveWeapon()
{
	GET_NETVAR(S_ENC("DT_BaseCombatCharacter"), S_ENC("m_hActiveWeapon"));
	return static_cast<C_BaseCombatWeapon*>(
		i::ClientEntList->getClientEntityFromHandle(
			getValue<hl::CHandle<IClientEntity>>(this, netvar)));
}

int	C_CSPlayer::getHealth()
{
	GET_NETVAR(S_ENC("DT_CSPlayer"), S_ENC("m_iHealth"));
	return getValue<int>(this, netvar);
}

bool C_CSPlayer::isAlive()
{
	GET_NETVAR(S_ENC("DT_CSPlayer"), S_ENC("m_lifeState"));
	return getValue<int>(this, netvar) == 0;
}

int	C_CSPlayer::getTeamNum()
{
	GET_NETVAR(S_ENC("DT_CSPlayer"), S_ENC("m_iTeamNum"));
	return getValue<int>(this, netvar);
}

int	C_CSPlayer::getFlags()
{
	GET_NETVAR(S_ENC("DT_CSPlayer"), S_ENC("m_fFlags"));
	return getValue<int>(this, netvar);
}

Vector C_CSPlayer::getViewOffset()
{
	GET_NETVAR(S_ENC("DT_CSPlayer"), S_ENC("m_vecViewOffset[0]"));
	return getValue<Vector>(this, netvar);
}

Vector C_CSPlayer::getOrigin()
{
	GET_NETVAR(S_ENC("DT_BasePlayer"), S_ENC("m_vecOrigin"));
	return getValue<Vector>(this, netvar);
}

Vector C_CSPlayer::getEyePos()
{
	return getOrigin() + getViewOffset();
}

Vector* C_CSPlayer::viewPunch()
{
	GET_NETVAR(S_ENC("DT_BasePlayer"), S_ENC("m_viewPunchAngle"));
	return getPointer<Vector>(this, netvar);
}

Vector* C_CSPlayer::aimPunch()
{
	GET_NETVAR(S_ENC("DT_BasePlayer"), S_ENC("m_aimPunchAngle"));
	return getPointer<Vector>(this, netvar);
}

player_info_t C_CSPlayer::getPlayerInfo()
{
	player_info_t pinfo;
	i::Engine->getPlayerInfo(this->entIndex(), &pinfo);
	return pinfo;
}

std::string C_CSPlayer::getName()
{
	return this->getPlayerInfo().name;
}

matrix3x4_t C_CSPlayer::entityToWorldTransform()
{
	return getValue<matrix3x4_t>(this, 0x440);
}