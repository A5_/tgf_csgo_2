#pragma once

#include "../Netvar.h"
#include "IClientEntity.h"
#include "C_BaseCombatWeapon.h"

class C_BaseCombatWeapon;

class C_CSPlayer : public hl::IClientEntity, NetvarClass
{
public:
	static C_CSPlayer*	getLocal();
	C_BaseCombatWeapon* getActiveWeapon();
	int		getHealth();
	bool	isAlive();
	int		getTeamNum();
	int		getFlags();
	Vector	getViewOffset();
	Vector	getOrigin();
	Vector	getEyePos();
	Vector* viewPunch();
	Vector* aimPunch();
	player_info_t getPlayerInfo();
	std::string getName();
	matrix3x4_t entityToWorldTransform();
};