#pragma once

#include"IClientNetworkable.h"
#include"IClientRenderable.h"
#include"IClientUnknown.h"
#include"IClientThinkable.h"

namespace hl
{
	struct SpatializationInfo_t;

	class IClientEntity : public IClientUnknown, public IClientRenderable, public IClientNetworkable, public IClientThinkable
	{
	public:
		virtual void             release() = 0;
		virtual const Vector     getAbsOrigin() const = 0;
		virtual const QAngle     getAbsAngles() const = 0;
		virtual void*            getMouth() = 0;
		virtual bool             getSoundSpatialization(SpatializationInfo_t info) = 0;
		virtual bool             isBlurred() = 0;
	};
}