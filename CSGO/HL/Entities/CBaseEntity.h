#pragma once

#include "../Netvar.h"
#include "IClientEntity.h"
#include "../Definitions/ClassIds.h"

class CBaseEntity : public hl::IClientEntity, NetvarClass
{
public:
	bool compareClientID(ClassIds id);
	Vector getOrigin();
	matrix3x4_t entityToWorldTransform();
	int getOwner();
	bool isWeapon();
};