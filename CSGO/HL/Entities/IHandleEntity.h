#pragma once

namespace hl
{
	class CBaseHandle;

	class IHandleEntity
	{
	public:
		virtual ~IHandleEntity() {}
		virtual void setRefHandle(const CBaseHandle &handle) = 0;
		virtual const CBaseHandle& getRefHandle() const = 0;
	};
}