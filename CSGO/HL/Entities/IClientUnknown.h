#pragma once

#include "IHandleEntity.h"

namespace hl
{
	class ICollideable
	{
	public:
		virtual void pad0();
		virtual const Vector& obbMin() const;
		virtual const Vector& obbMax() const;
	};

	class IClientNetworkable;
	class IClientRenderable;
	class IClientEntity;
	class C_BaseEntity;
	class IClientThinkable;
	class IClientAlphaProperty;

	class IClientUnknown : public IHandleEntity
	{
	public:
		virtual ICollideable*              getCollideable() = 0;
		virtual IClientNetworkable*        getClientNetworkable() = 0;
		virtual IClientRenderable*         getClientRenderable() = 0;
		virtual IClientEntity*             getIClientEntity() = 0;
		virtual C_BaseEntity*              getBaseEntity() = 0;
		virtual IClientThinkable*          getClientThinkable() = 0;
		//virtual IClientModelRenderable*  getClientModelRenderable() = 0;
		virtual IClientAlphaProperty*      getClientAlphaProperty() = 0;
	};
}