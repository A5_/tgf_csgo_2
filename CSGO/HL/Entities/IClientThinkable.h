#pragma once

namespace hl
{
	class IClientUnknown;
	class CClientThinkHandlePtr;
	typedef CClientThinkHandlePtr* ClientThinkHandle_t;

	class IClientThinkable
	{
	public:
		virtual IClientUnknown*		getIClientUnknown() = 0;
		virtual void				clientThink() = 0;
		virtual ClientThinkHandle_t	getThinkHandle() = 0;
		virtual void				setThinkHandle(ClientThinkHandle_t hThink) = 0;
		virtual void				release() = 0;
	};
}