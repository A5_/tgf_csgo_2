#include "C_BaseCombatWeapon.h"
#include "../Utl/CHandle.h"

#include <algorithm>

C_CSPlayer* C_BaseCombatWeapon::getOwner()
{
	GET_NETVAR(S_ENC("DT_BaseEntity"), S_ENC("m_hOwnerEntity"));
	return static_cast<C_CSPlayer*>(
		i::ClientEntList->getClientEntityFromHandle(
			getValue<hl::CHandle<C_CSPlayer>>(this, netvar)));
}

float C_BaseCombatWeapon::nextPrimaryAttack()
{
	GET_NETVAR(S_ENC("DT_BaseCombatWeapon"), S_ENC("m_flNextPrimaryAttack"));
	return getValue<float>(this, netvar);
}

int C_BaseCombatWeapon::getId()
{
	using Fn = int(__thiscall*)(void*);
	return getMethod<Fn>(this, 458)(this);
}

std::string C_BaseCombatWeapon::getName()
{
	using Fn = const char*(__thiscall*)(void*);
	std::string name = getMethod<Fn>(this, 378)(this);
	name.erase(0, 7);
	if(name.length() <= 4)
	{
		std::transform(
			name.begin(), name.end(), name.begin(), toupper);
	}
	else
	{
		std::transform(
			name.begin(), name.begin() + 1, name.begin(), toupper);
	}

	return name;
}