#pragma once
#include "C_CSPlayer.h"

class C_CSPlayer;

class C_BaseCombatWeapon : public hl::IClientEntity, NetvarClass
{
public:
	C_CSPlayer* getOwner();
	float		nextPrimaryAttack();
	int			getId();
	std::string getName();
};