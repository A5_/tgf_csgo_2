#pragma once
class ClientClass;

namespace hl
{
	class IClientUnknown;
	class bf_read;

	class IClientNetworkable
	{
	public:
		virtual IClientUnknown*  getIClientUnknown() = 0;
		virtual void             release() = 0;
		virtual ClientClass*     getClientClass() = 0;
		virtual void             notifyShouldTransmit(int state) = 0;
		virtual void             onPreDataChanged(int updateType) = 0;
		virtual void             onDataChanged(int updateType) = 0;
		virtual void             preDataUpdate(int updateType) = 0;
		virtual void             postDataUpdate(int updateType) = 0;
		virtual void             __unkn() = 0;
		virtual bool             isDormant() = 0;
		virtual int              entIndex() const = 0;
		virtual void             receiveMessage(int classID, bf_read& msg) = 0;
		virtual void*            getDataTableBasePtr() = 0;
		virtual void             setDestroyedOnRecreateEntities() = 0;
	};
}