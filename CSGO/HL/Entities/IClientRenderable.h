#pragma once

#include "../Mathlib/QAngle.h"
#include "../Definitions/ModelRenderInfo.h"

namespace hl
{
	typedef unsigned short ClientShadowHandle_t;
	typedef unsigned short ClientRenderHandle_t;
	typedef unsigned short ModelInstanceHandle_t;
	typedef unsigned char uint8;

	class matrix3x4_t;
	class IClientUnknown;

	class IClientRenderable
	{
	public:
		virtual IClientUnknown*            getIClientUnknown() = 0;
		virtual Vector const&              getRenderOrigin() = 0;
		virtual QAngle const&              getRenderAngles() = 0;
		virtual bool                       shouldDraw() = 0;
		virtual int                        getRenderFlags() = 0; // ERENDERFLAGS_xxx
		virtual void                       unused() const {}
		virtual ClientShadowHandle_t       getShadowHandle() const = 0;
		virtual ClientRenderHandle_t&      renderHandle() = 0;
		virtual const model_t*             getModel() const = 0;
		virtual int                        drawModel(int flags, const int /*RenderableInstance_t*/ &instance) = 0;
		virtual int                        getBody() = 0;
		virtual void                       getColorModulation(float* color) = 0;
		virtual bool                       lodTest() = 0;
		virtual bool                       setupBones(matrix3x4_t *pBoneToWorldOut, int nMaxBones, int boneMask, float currentTime) = 0;
		virtual void                       setupWeights(const matrix3x4_t *pBoneToWorld, int nFlexWeightCount, float *pFlexWeights, float *pFlexDelayedWeights) = 0;
		virtual void                       doAnimationEvents() = 0;
		virtual void* /*IPVSNotify*/       getPVSNotifyInterface() = 0;
		virtual void                       getRenderBounds(Vector& mins, Vector& maxs) = 0;
		virtual void                       getRenderBoundsWorldspace(Vector& mins, Vector& maxs) = 0;
		virtual void                       getShadowRenderBounds(Vector &mins, Vector &maxs, int /*ShadowType_t*/ shadowType) = 0;
		virtual bool                       shouldReceiveProjectedTextures(int flags) = 0;
		virtual bool                       getShadowCastDistance(float *pDist, int /*ShadowType_t*/ shadowType) const = 0;
		virtual bool                       getShadowCastDirection(Vector *pDirection, int /*ShadowType_t*/ shadowType) const = 0;
		virtual bool                       isShadowDirty() = 0;
		virtual void                       markShadowDirty(bool bDirty) = 0;
		virtual IClientRenderable*         getShadowParent() = 0;
		virtual IClientRenderable*         firstShadowChild() = 0;
		virtual IClientRenderable*         nextShadowPeer() = 0;
		virtual int /*ShadowType_t*/       shadowCastType() = 0;
		virtual void                       createModelInstance() = 0;
		virtual ModelInstanceHandle_t      getModelInstance() = 0;
		virtual const matrix3x4_t&         renderableToWorldTransform() = 0;
		virtual int                        lookupAttachment(const char *pAttachmentName) = 0;
		virtual   bool                     getAttachment(int number, Vector &origin, QAngle &angles) = 0;
		virtual bool                       getAttachment(int number, matrix3x4_t &matrix) = 0;
		virtual float*                     getRenderClipPlane() = 0;
		virtual int                        getSkin() = 0;
		virtual void                       onThreadedDrawSetup() = 0;
		virtual bool                       usesFlexDelayedWeights() = 0;
		virtual void                       recordToolMessage() = 0;
		virtual bool                       shouldDrawForSplitScreenUser(int nSlot) = 0;
		virtual uint8                      overrideAlphaModulation(uint8 nAlpha) = 0;
		virtual uint8                      overrideShadowAlphaModulation(uint8 nAlpha) = 0;
	};
}