#include "CBaseEntity.h"

bool CBaseEntity::compareClientID(ClassIds id)
{
	if(this == nullptr) return false;
	return (this->getClientClass()->classID == id) ? 
		true : false;
}

Vector CBaseEntity::getOrigin()
{
	GET_NETVAR(S_ENC("DT_BasePlayer"), S_ENC("m_vecOrigin"));
	return getValue<Vector>(this, netvar);
}

matrix3x4_t CBaseEntity::entityToWorldTransform()
{
	return getValue<matrix3x4_t>(this, 0x440);
}

int CBaseEntity::getOwner()
{
	GET_NETVAR(C_ENC("DT_BaseEntity"), C_ENC("m_hOwnerEntity"));
	return getValue<int>(this, netvar);
}

bool CBaseEntity::isWeapon()
{
	if(!this) return false;

	auto class_ = this->getClientClass();
	if(!class_) return false;

	auto id = ClassIds(class_->classID);

	switch(id)
	{
	case CAK47:
	case CDEagle:
	case CWeaponAug:
	case CWeaponAWP:
	case CWeaponBizon:
	case CWeaponElite:
	case CWeaponFamas:
	case CWeaponFiveSeven:
	case CWeaponG3SG1:
	case CWeaponGalil:
	case CWeaponGalilAR:
	case CWeaponGlock:
	case CWeaponHKP2000:
	case CWeaponM249:
	case CWeaponM3:
	case CWeaponM4A1:
	case CWeaponMAC10:
	case CWeaponMag7:
	case CWeaponMP7:
	case CWeaponMP9:
	case CWeaponNegev:
	case CWeaponNOVA:
	case CWeaponP228:
	case CWeaponP250:
	case CWeaponP90:
	case CWeaponSawedoff:
	case CWeaponSCAR20:
	case CWeaponScout:
	case CWeaponSG556:
	case CWeaponSSG08:
	case CWeaponTaser:
	case CWeaponTec9:
	case CWeaponUMP45:
	case CWeaponUSP:
	case CWeaponXM1014:
	case CKnife:
		return true;
	default:
		return false;
	}
}

