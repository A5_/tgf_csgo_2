#include "CVar.h"

hl::ConVar* ICVar::findVar(const char *name)
{
	using Fn = hl::ConVar*(__thiscall*)(void*, const char*);
	return hl::callMethod<Fn>(this, 16)(this, name);
}