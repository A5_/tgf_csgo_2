#pragma once

#include "../../Utils/VMTClass.h"
#include "../Definitions/Playerinfo.h"
#include "../Mathlib/Vector.h"
#include "../Mathlib/QAngle.h"
#include "../Mathlib/VMatrix.h"

class IEngine
{
public:
	void		getViewAngles(QAngle& angles);
	void		setViewAngles(QAngle& angles);
	bool		getPlayerInfo(int ent, player_info_t* info);
	int			getLocalPlayer();
	bool		isInGame();
	VMatrix&	worldToScreenMatrix();
	void		getScreenSize(int& width, int& height);
	bool		screenTransform(const Vector &point, Vector &screen);
	bool		worldToScreen(const Vector& in, Vector& out);
	void		clientCmd(const char *cmd);
};