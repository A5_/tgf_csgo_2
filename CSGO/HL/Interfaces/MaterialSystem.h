#pragma once
#include "../Material/IMaterial.h"

class KeyValues;

class IMaterialSystem
{
public:
	IMaterial* createMaterial(const char* name, KeyValues* vk);
	IMaterial* findMaterial(char const* pMaterialName, const char *pTextureGroupName, bool complain = true, const char *pComplainPrefix = 0);
};