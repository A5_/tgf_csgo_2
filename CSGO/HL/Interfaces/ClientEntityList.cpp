#include "ClientEntityList.h"

hl::IClientEntity* IClientEntityList::getClientEntity(int index)
{
	using Fn = hl::IClientEntity*(__thiscall*)(void*, int);
	return hl::callMethod<Fn>(this, 3)(this, index);
}

hl::IClientEntity* IClientEntityList::getClientEntityFromHandle(hl::CBaseHandle handle)
{
	using Fn = hl::IClientEntity*(__thiscall*)(void*, hl::CBaseHandle);
	return hl::callMethod<Fn>(this,4)(this, handle);
}

int IClientEntityList::numberOfEntities(bool includeNonNetworkable)
{
	using Fn = int(__thiscall*)(void*, bool);
	return hl::callMethod<Fn>(this,5)(this, includeNonNetworkable);
}

int IClientEntityList::getHighestEntityIndex()
{
	using Fn = int(__thiscall*)(void*);
	return hl::callMethod<Fn>(this,6)(this);
}