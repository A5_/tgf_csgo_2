#include "MaterialSystem.h"
#include "../SDK.h"

IMaterial* IMaterialSystem::createMaterial(const char* name, KeyValues* vk)
{
	using Fn = IMaterial*(__thiscall*)(void*, const char*, KeyValues*);
	return hl::callMethod<Fn>(this, 83)(this, name, vk);
}

IMaterial* IMaterialSystem::findMaterial(char const* matname, const char* groupname, bool complain, const char *complaintext)
{
	using Fn = IMaterial*(__thiscall*)(void*, char const*, const char*, bool, const char*);
	return hl::callMethod<Fn>(this, 84)(this, matname, groupname, complain, complaintext);
}
