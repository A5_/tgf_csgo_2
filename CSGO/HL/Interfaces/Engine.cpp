#include "Engine.h"

void IEngine::getViewAngles(QAngle& angles)
{
	using Fn = void(__thiscall*)(void*, Vector&);
	hl::callMethod<Fn>(this, 18)(this, angles);
}

void IEngine::setViewAngles(QAngle& angles)
{
	using Fn = void(__thiscall*)(void*, Vector&);
	hl::callMethod<Fn>(this, 19)(this, angles);
}

bool IEngine::getPlayerInfo(int ent, player_info_t* info)
{
	using Fn = bool(__thiscall*)(void*, int, player_info_t*);
	return hl::callMethod<Fn>(this, 8)(this, ent, info);
}

int IEngine::getLocalPlayer()
{
	using Fn = int(__thiscall*)(void*);
	return hl::callMethod<Fn>(this, 12)(this);
}

bool IEngine::isInGame()
{
	using Fn = bool(__thiscall*)(void*);
	return hl::callMethod<Fn>(this, 26)(this);
}

VMatrix& IEngine::worldToScreenMatrix()
{
	using Fn = VMatrix&(__thiscall*)(void*);
	return hl::callMethod<Fn>(this, 37)(this);
}

void IEngine::getScreenSize(int& width, int& height)
{
	using Fn = void(__thiscall*)(void*, int&, int&);
	hl::callMethod<Fn>(this, 5)(this, width, height);
}

bool IEngine::screenTransform(const Vector &point, Vector &screen)
{
	float w;
	const VMatrix &worldToScreen = this->worldToScreenMatrix();

	screen.x = worldToScreen[0][0] * point[0] + worldToScreen[0][1] * point[1] + worldToScreen[0][2] * point[2] + worldToScreen[0][3];
	screen.y = worldToScreen[1][0] * point[0] + worldToScreen[1][1] * point[1] + worldToScreen[1][2] * point[2] + worldToScreen[1][3];
	w = worldToScreen[3][0] * point[0] + worldToScreen[3][1] * point[1] + worldToScreen[3][2] * point[2] + worldToScreen[3][3];
	screen.z = 0.0f;

	bool behind = false;

	if(w < 0.001f)
	{
		behind = true;
		screen.x *= 100000;
		screen.y *= 100000;
	}
	else
	{
		behind = false;
		float invw = 1.0f / w;
		screen.x *= invw;
		screen.y *= invw;
	}

	return behind;
}

bool IEngine::worldToScreen(const Vector& in, Vector& out)
{
	if(!this->screenTransform(in, out))
	{
		int screenWidth, screenHeight;
		this->getScreenSize(screenWidth, screenHeight);
		float x = screenWidth / 2;
		float y = screenHeight / 2;
		x += 0.5 * out.x * screenWidth + 0.5;
		y -= 0.5 * out.y * screenHeight + 0.5;
		out.x =  x;
		out.y =  y;
		return true;
	}

	return false;
}

void IEngine::clientCmd(const char *cmd)
{
	using Fn = void(__thiscall*)(void*, const char*, const char*);
	hl::callMethod<Fn>(this, 114)(this, cmd, 0);
}