#pragma once

#include "../../Utils/VMTClass.h"
#include "../Entities/IClientEntity.h"
#include "../Utl/CHandle.h"

class IClientEntityList
{
public:
	hl::IClientEntity*	getClientEntity(int index);
	hl::IClientEntity*	getClientEntityFromHandle(hl::CBaseHandle handle);
	int					numberOfEntities(bool includeNonNetworkable);
	int					getHighestEntityIndex();
};

