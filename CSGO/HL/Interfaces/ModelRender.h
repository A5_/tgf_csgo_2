#pragma once
#include "../SDK.h"
#include "../Material/IMaterial.h"

class IModelRender : public VMTClass
{
public:
	void forcedMaterialOverride(void* thisptr, IMaterial *material, int type = 0, int idk = 0)
	{
		typedef void(__thiscall* Fn)(void*, IMaterial*, int, int);
		return hl::callMethod<Fn>(thisptr, 1)(thisptr, material, type, idk);
	}
};