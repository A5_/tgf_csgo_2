#pragma once

#include "../Utl/Convar.h"
#include "../../Utils/VMTClass.h"

class ICVar
{
public:
	hl::ConVar* findVar(const char *name);
};