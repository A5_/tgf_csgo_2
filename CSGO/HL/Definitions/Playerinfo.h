#pragma once

typedef struct player_info_s
{
	char	pad_0[0x8];
	int		xuidlow;
	int		xuidhigh;
	char	name[128];
	int		userid;
	char	guid[33];
	char	pad_1[0x17B];
} player_info_t;  