#pragma once
#include "Mathlib/Vector.h"

#include "../Utils/VMTClass.h"

#include "../HL/Interfaces/CVar.h"
#include "../HL/Interfaces/Engine.h"
#include "../HL/Interfaces/ClientEntityList.h"
#include "../HL/Interfaces/ModelRender.h"
#include "../HL/Interfaces/ModelInfo.h"
#include "../HL/Interfaces/MaterialSystem.h"
#include "../HL/Interfaces/RenderView.h"

#include "Definitions/ClassIds.h"

using byte = unsigned char;

#define CONCAT_IMPL(x, y) x##y
#define MACRO_CONCAT(x, y) CONCAT_IMPL(x, y)
#define PAD(size) byte MACRO_CONCAT(_pad, __COUNTER__)[size];

#define IMPLEMENT_OPERATOR_EQUAL( _classname )			\
	public:												\
		_classname &operator=( const _classname &src )	\
		{												\
			memcpy( this, &src, sizeof(_classname) );	\
			return *this;								\
		}

namespace i
{
	extern VMTClass Client;
	extern VMTClass ClientMode;
	extern IClientEntityList* ClientEntList;
	extern IEngine* Engine;
	extern ICVar* CVar;
	extern VMTClass EngineTrace;
	extern VMTClass Globals;
	extern IModelRender ModelRender;
	extern IVModelInfo* ModelInfo;
	extern IMaterialSystem* MaterialSystem;
	extern VMTClass Material;
	extern IVRenderView* RenderView;
	extern VMTClass Prediction;
	extern VMTClass Physprops;
	extern VMTClass DebugOverlay;
	extern VMTClass StudioRender;
	extern VMTClass D3DDevice9;
}

class DVariant
{
public:
	union
	{
		float _float;
		long _int;
		char* _string;
		void* _data;
		Vector _vector;
	};
};

struct RecvProp;

class CRecvProxyData
{
public:
	const RecvProp* recvProp;
	DVariant value;
	int element;
	int objectID;
};

using RecvVarProxyFn = void(*)( const CRecvProxyData* data, void* info, void* out );

struct RecvTable
{
	RecvProp* prop;
	int numProps;
	void* decoder;
	char* tableName;
	bool initialized;
	bool inMainList;
};

struct RecvProp
{
	char* varName;
	int recvType;
	int flags;
	int bufferSize;
	bool inArray;
	const void* extraData;
	RecvProp* arrayProp;
	void* arrayLengthProxy;
	void* proxyFn;
	void* dataTableProxyFn;
	RecvTable* dataTable;
	int offset;
	int elementStride;
	int numElements;
	const char* parentArrayPropName;
};

class ClientClass
{
public:
	void* createFn;
	void* createEventFn;
	char* networkName;
	RecvTable* recvTable;
	ClientClass* next;
	int classID;
};

class CUserCmd
{
public:
	virtual ~CUserCmd() { };
	int commandNumber;
	int ticCount;
	QAngle viewAngles;
	Vector aimDirection;
	float forwardMove;
	float sideMove;
	float upMove;
	int buttons;
	byte impulse;
	int weaponSelect;
	int weaponSubtype;
	int randomSeed;
	short mousedX;
	short mousedY;
	bool hasBeenPredicted;
private:
	PAD(0x18);
};

class KeyValues
{
public:
	char pad_0[0x20];
};

#define TEXTURE_GROUP_LIGHTMAP						C_ENC("Lightmaps")
#define TEXTURE_GROUP_WORLD							C_ENC("World textures")
#define TEXTURE_GROUP_MODEL							C_ENC("Model textures")
#define TEXTURE_GROUP_VGUI							C_ENC("VGUI textures")
#define TEXTURE_GROUP_PARTICLE						C_ENC("Particle textures")
#define TEXTURE_GROUP_DECAL							C_ENC("Decal textures")
#define TEXTURE_GROUP_SKYBOX						C_ENC("SkyBox textures")
#define TEXTURE_GROUP_CLIENT_EFFECTS				C_ENC("ClientEffect textures")
#define TEXTURE_GROUP_OTHER							C_ENC("Other textures")
#define TEXTURE_GROUP_PRECACHED						C_ENC("Precached")
#define TEXTURE_GROUP_CUBE_MAP						C_ENC("CubeMap textures")
#define TEXTURE_GROUP_RENDER_TARGET					C_ENC("RenderTargets")
#define TEXTURE_GROUP_UNACCOUNTED					C_ENC("Unaccounted textures")
#define TEXTURE_GROUP_STATIC_INDEX_BUFFER			C_ENC("Static Indices")
#define TEXTURE_GROUP_STATIC_VERTEX_BUFFER_DISP		C_ENC("Displacement Verts")
#define TEXTURE_GROUP_STATIC_VERTEX_BUFFER_COLOR	C_ENC("Lighting Verts")
#define TEXTURE_GROUP_STATIC_VERTEX_BUFFER_WORLD	C_ENC("World Verts")
#define TEXTURE_GROUP_STATIC_VERTEX_BUFFER_MODELS	C_ENC("Model Verts")
#define TEXTURE_GROUP_STATIC_VERTEX_BUFFER_OTHER	C_ENC("Other Verts")
#define TEXTURE_GROUP_DYNAMIC_INDEX_BUFFER			C_ENC("Dynamic Indices")
#define TEXTURE_GROUP_DYNAMIC_VERTEX_BUFFER			C_ENC("Dynamic Verts")
#define TEXTURE_GROUP_DEPTH_BUFFER					C_ENC("DepthBuffer")
#define TEXTURE_GROUP_VIEW_MODEL					C_ENC("ViewModel")
#define TEXTURE_GROUP_PIXEL_SHADERS					C_ENC("Pixel Shaders")
#define TEXTURE_GROUP_VERTEX_SHADERS				C_ENC("Vertex Shaders")
#define TEXTURE_GROUP_RENDER_TARGET_SURFACE			C_ENC("RenderTarget Surfaces")
#define TEXTURE_GROUP_MORPH_TARGETS					C_ENC("Morph Targets")

struct studiohwdata_t;
struct StudioDecalHandle_t;
struct MaterialLightingState_t;

class IMesh;
class IPooledVBAllocator;

struct ColorMeshInfo_t
{
	// A given color mesh can own a unique Mesh, or it can use a shared Mesh
	// (in which case it uses a sub-range defined by m_nVertOffset and m_nNumVerts)
	IMesh* m_pMesh;
	IPooledVBAllocator* m_pPooledVBAllocator;
	int m_nVertOffsetInBytes;
	int m_nNumVerts;
};

struct DrawModelInfo_t
{
	studiohdr_t* m_pStudioHdr;
	studiohwdata_t* m_pHardwareData;
	StudioDecalHandle_t* m_Decals;
	int m_Skin;
	int m_Body;
	int m_HitboxSet;
	void* m_pClientEntity;
	int m_Lod;
	ColorMeshInfo_t* m_pColorMeshes;
	bool m_bStaticLighting;
	MaterialLightingState_t* m_LightingState;

public:
	DrawModelInfo_t& operator=( const DrawModelInfo_t& src )
	{
		memcpy( this, &src, sizeof(DrawModelInfo_t) );
		return *this;
	};
};

enum
{
	STUDIORENDER_DRAW_ENTIRE_MODEL = 0x000,
	STUDIORENDER_DRAW_OPAQUE_ONLY = 0x001,
	STUDIORENDER_DRAW_TRANSLUCENT_ONLY = 0x002,
	STUDIORENDER_DRAW_GROUP_MASK = 0x003,
	STUDIORENDER_DRAW_NO_FLEXES = 0x004,
	STUDIORENDER_DRAW_STATIC_LIGHTING = 0x008,
	STUDIORENDER_DRAW_ACCURATETIME = 0x010,
	STUDIORENDER_DRAW_NO_SHADOWS = 0x020,
	STUDIORENDER_DRAW_GET_PERF_STATS = 0x040,
	STUDIORENDER_DRAW_WIREFRAME = 0x080,
	STUDIORENDER_DRAW_ITEM_BLINK = 0x100,
	STUDIORENDER_SHADOWDEPTHTEXTURE = 0x200,
	STUDIORENDER_NO_SKIN = 0x400,
	STUDIORENDER_SKIP_DECALS = 0x800
};
