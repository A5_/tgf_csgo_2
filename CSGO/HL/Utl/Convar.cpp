#include "ConVar.h"
#include "../SDK.h"

void hl::ConVar::setValue(const char* value)
{
	using Fn = void(__thiscall*)(void*, const char*);
	return hl::callMethod<Fn>(this, 14)(this, value);
}

void hl::ConVar::setValue(float value)
{
	using Fn = void(__thiscall*)(void*, float);
	return hl::callMethod<Fn>(this, 15)(this, value);
}

void hl::ConVar::setValue(int value)
{
	using Fn = void(__thiscall*)(void*, int);
	return hl::callMethod<Fn>(this, 16)(this, value);
}

void hl::ConVar::setValue(Color value)
{
	using Fn = void(__thiscall*)(void*, Color);
	return hl::callMethod<Fn>(this, 17)(this, value);
}

char* hl::ConVar::getName()
{
	using Fn = char*(__thiscall*)(void*);
	return hl::callMethod<Fn>(this, 5)(this);
}

char* hl::ConVar::getDefault()
{
	return defaultValue;
}