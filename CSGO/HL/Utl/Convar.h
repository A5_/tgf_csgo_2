#pragma once
#include "Color.h"

namespace hl
{
	class ConVar
	{
	public:
		void setValue(const char *value);
		void setValue(float value);
		void setValue(int value);
		void setValue(Color value);
		char* getName();
		char* getDefault();
	private:
		byte pad_1[4];
	public:
		ConVar* next;
		__int32 registered;
		char* nameText_;
		char* helpString;
		__int32 flags;
	private:
		byte pad_2[4];
	public:
		ConVar* parent;
		char* defaultValue;
		char* string;
		__int32 stringLength;
		float fValue;
		__int32 nValue;
		__int32 hasMin;
		float minVal;
		__int32 hasMax;
		float maxVal;
		void* changeCallback;
	};
}