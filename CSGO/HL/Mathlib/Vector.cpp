#include "Vector.h"

#include <cfloat>
#include <cmath>

Vector::Vector()
{
	x = y = z = 0.0f;
}

Vector::Vector(float _x, float _y, float _z)
{
	x = _x;
	y = _y;
	z = _z;
}

Vector& Vector::operator = (const Vector& in)
{
	x = in.x;
	y = in.y;
	z = in.z;

	return *this;
}

Vector& Vector::operator += (const Vector& in)
{
	x += in.x;
	y += in.y;
	z += in.z;

	return *this;
}

Vector& Vector::operator -= (const Vector& in)
{
	x -= in.x;
	y -= in.y;
	z -= in.z;

	return *this;
}

Vector& Vector::operator *= (const Vector& in)
{
	x *= in.x;
	y *= in.y;
	z *= in.z;

	return *this;
}

Vector& Vector::operator *= (const float in)
{
	x *= in;
	y *= in;
	z *= in;

	return *this;
}

Vector Vector::operator + (const Vector& in) const
{
	Vector temp;

	temp.x = x + in.x;
	temp.y = y + in.y;
	temp.z = z + in.z;

	return temp;
}

Vector Vector::operator - (const Vector& in) const
{
	Vector temp;

	temp.x = x - in.x;
	temp.y = y - in.y;
	temp.z = z - in.z;

	return temp;
}

Vector Vector::operator / (const Vector& in) const
{
	Vector temp;

	temp.x = x / (in.x == 0.0f ? FLT_EPSILON : in.x);
	temp.y = y / (in.y == 0.0f ? FLT_EPSILON : in.y);
	temp.z = z / (in.x == 0.0f ? FLT_EPSILON : in.z);

	return temp;
}

Vector Vector::operator / (const float& in) const
{
	Vector temp;

	float flDiv = in == 0.0f ? FLT_EPSILON : in;

	temp.x = x / flDiv;
	temp.y = y / flDiv;
	temp.z = z / flDiv;

	return temp;
}

Vector Vector::operator * (const Vector& in) const
{
	Vector temp;

	temp.x = x * in.x;
	temp.y = y * in.y;
	temp.z = z * in.z;

	return temp;
}

Vector Vector::operator * (const float& in) const
{
	Vector temp;

	temp.x = x * in;
	temp.y = y * in;
	temp.z = z * in;

	return temp;
}

bool Vector::operator == (const Vector& in) const
{
	if(fabs(in.x - x) < FLT_EPSILON &&
	   fabs(in.y - y) < FLT_EPSILON &&
	   fabs(in.z - z) < FLT_EPSILON)
	{
		return true;
	}

	return false;
}

bool Vector::operator != (const Vector& in) const
{
	if(fabs(in.x - x) > FLT_EPSILON &&
	   fabs(in.y - y) > FLT_EPSILON &&
	   fabs(in.z - z) > FLT_EPSILON)
	{
		return true;
	}

	return false;
}

float& Vector::operator [] (int index)
{
	return (&x)[index];
}

float Vector::operator [] (int index) const
{
	return (&x)[index];
}

float Vector::length() const
{
	return fastSqrt(x*x + y*y + z*z);
}

float Vector::lengthXY() const
{
	return fastSqrt(x*x + y*y);
}

float Vector::lengthXZ() const
{
	return fastSqrt(x*x + z*z);
}

float Vector::normalise()
{
	auto flLength = length();

	auto flLengthNormal = 1.0f / (FLT_EPSILON + flLength);

	x = x * flLengthNormal;
	y = y * flLengthNormal;
	z = z * flLengthNormal;

	return flLength;
}

float Vector::dotProduct(const Vector& a) const
{
	return this->x * a.x + this->y * a.y + this->z * a.z;
}

inline float  Vector::dotProduct(const float* a) const
{
	return this->x * a[0] + this->y * a[1] + this->z * a[2];
}


Vector Vector::crossProduct(const Vector& vIn) const
{
	return Vector(this->y * vIn.z - this->z * vIn.y,
				  this->z * vIn.x - this->x * vIn.z,
				  this->x * vIn.y - this->y * vIn.x);
}

Vector Vector::transform(const matrix3x4_t& in) const
{
	Vector out;
	out.x = dotProduct(in[0]) + in[0][3];
	out.y = dotProduct(in[1]) + in[1][3];
	out.z = dotProduct(in[2]) + in[2][3];
	return out;
}

float Vector::distance(const Vector& v) const
{
	return fastSqrt(pow(x - v.x, 2) + pow(y - v.y, 2) + pow(z - v.z, 2));
}