#pragma once

#include "Vector.h"

class QAngle : public Vector
{
public:
	inline QAngle clamp();
	static QAngle angleNormalize(QAngle vAngle, float flMin, float flMax);
};