#pragma once

#include "VMatrix.h"
#include "Inline.h"


class Vector
{
public:
	Vector();
	Vector(float _x, float _y, float _z);

	Vector&	operator = (const Vector&);
	Vector&	operator +=(const Vector&);
	Vector&	operator -=(const Vector&);
	Vector&	operator *=(const Vector&);
	Vector&	operator *=(const float  );
	Vector	operator + (const Vector&) const;
	Vector	operator - (const Vector&) const;
	Vector	operator / (const Vector&) const;
	Vector	operator / (const float& ) const;
	Vector	operator*(const Vector& in) const;
	Vector	operator*(const float& in) const;
	bool	operator==(const Vector& in) const;
	bool	operator!=(const Vector& in) const;
	float&	operator[](int index);
	float	operator[](int index) const;

	inline float  length() const;
	inline float  lengthXY() const;
	inline float  lengthXZ() const;
	inline float  dotProduct(const Vector&) const;
	inline float  dotProduct(const float*) const;
	inline Vector crossProduct(const Vector&) const;
	inline float  normalise();
	Vector transform(const matrix3x4_t& in) const;
	float  distance(const Vector&) const;


	float x;
	float y;
	float z;
};

class VectorAligned : public Vector
{
public:
	VectorAligned()
	{
		x = y = z = 0;
	}

	explicit VectorAligned(const Vector& in)
	{
		x = in.x;
		y = in.y;
		z = in.z;
	}
};