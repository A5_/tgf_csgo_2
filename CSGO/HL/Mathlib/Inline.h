#pragma once

#define MOD 1000000007

inline auto fastSqrt(float x)
{
	auto root = 0.0f;

	__asm sqrtss xmm0, x
	__asm movss root, xmm0

	return root;
}