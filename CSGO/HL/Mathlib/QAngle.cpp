#include "QAngle.h"
QAngle QAngle::clamp()
{
	if(this->x < -89.0f) this->x = -89.0f;
	if(this->x > 89.0f) this->x = 89.0f;

	while(this->y < -180.0f) this->y += 360.0f;
	while(this->y > 180.0f) this->y -= 360.0f;

	this->z = 0.0f;

	return *this;
}

QAngle QAngle::angleNormalize(QAngle vAngles, float flMax, float flMin)
{
	auto flDelta = flMax - flMin;

	if(vAngles.x > flMax)	vAngles.x -= flDelta;
	if(vAngles.y > flMax)	vAngles.y -= flDelta;
	if(vAngles.x < -flMin)	vAngles.x += flDelta;
	if(vAngles.y < -flMin)	vAngles.y += flDelta;

	return vAngles;
}