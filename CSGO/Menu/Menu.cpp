#include "Menu.h"

#include "../Include.h"
#include "../ImGUI/Draw.h"

#include "../ImGUI/imgui.h"
#include "../ImGUI/imgui_tabs.h"
#include "../ImGUI/DX9/imgui_impl_dx9.h"

#include "../Globals/Options.h"
#include "../Utils/Config.h"

const char* chamArray[]
{
	"Flat",
	"Lit",
	"Wireframe"
};

void Menu::draw()
{
	gui::Begin( C_ENC("harmful.space"), &option::get< bool >( menu_visible ), 
		ImVec2( 600, 400 ), -1, ImGuiWindowFlags_ShowBorders | ImGuiWindowFlags_NoResize );
	{
		ImGuiTab tab;
		//tab.newTab(S_ENC("Aimbot"));
		//tab.newTab(S_ENC("Triggerbot"));
		tab.newTab( S_ENC("Visuals") );
		tab.newTab( S_ENC("Misc") );
		tab.Draw( &tab_ );

		switch( tab_ )
		{
			case Visuals:
				visuals();
				break;
			case Misc:
				misc();
				break;
			default:
				break;
		}
	}
	gui::End();
}

void Menu::aimbot() { }

void Menu::triggerbot() {}

void Menu::visuals()
{
	gui::Columns( 2 );
	{
		gui::Checkbox( C_ENC("ESP Toggle"), &option::get< bool >( esp_toggle ) );
		gui::Checkbox( C_ENC("Enemy"), &option::get< bool >( esp_enemy ) );
		gui::Checkbox( C_ENC("Friendly"), &option::get< bool >( esp_friendly ) );
		gui::Checkbox( C_ENC("Bounding Box"), &option::get< bool >( esp_box ) );
		gui::Checkbox( C_ENC("Player Name"), &option::get< bool >( esp_name ) );
		gui::Checkbox( C_ENC("Health Bar"), &option::get< bool >( esp_health ) );
		gui::Checkbox( C_ENC("Weapon Info"), &option::get< bool >( esp_item ) );
	}
	gui::NextColumn();
	{
		gui::Checkbox( C_ENC("Chams Toggle"), &option::get< bool >( chams_toggle ) );
		gui::Checkbox( C_ENC("XQZ"), &option::get< bool >( chams_xqz ) );
		gui::Combo( C_ENC("Mode"), &option::get< int >( chams_mode ), chamArray, IM_ARRAYSIZE(chamArray) );
	}
	gui::Columns( 1 );
}

void Menu::skinChanger() {}

void Menu::misc()
{
	if( gui::Button( C_ENC("Save Config") ) )
		Config::get().save();
	if( gui::Button( C_ENC("Load Config") ) )
		Config::get().load();
}
