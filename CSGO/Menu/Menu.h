#pragma once
#include <vector>

enum eTabs
{
	Visuals,
	Misc
};

class Menu
{
public:
	static Menu& get()
	{
		static Menu instance;
		return instance;
	}

	void draw();

private:
	void aimbot();
	void triggerbot();
	void visuals();
	void skinChanger();
	void misc();

	void setup();

	int tab_ = 0;
	bool setup_ = true;

	std::vector< const char* > chamsmode;
};
