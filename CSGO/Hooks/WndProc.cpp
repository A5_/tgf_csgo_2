#include "Hooks.h"

#include "../Globals/Options.h"

extern LRESULT ImGui_ImplDX9_WndProcHandler(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

void openMenu();

namespace hooks
{
	WNDPROC oWndProc;

	LRESULT __stdcall WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
	{
		switch(uMsg)
		{
		case WM_LBUTTONDOWN:
			g::pressed[VK_LBUTTON] = true;
			break;
		case WM_LBUTTONUP:
			g::pressed[VK_LBUTTON] = false;
			break;
		case WM_RBUTTONDOWN:
			g::pressed[VK_RBUTTON] = true;
			break;
		case WM_RBUTTONUP:
			g::pressed[VK_RBUTTON] = false;
			break;
		case WM_MBUTTONDOWN:
			g::pressed[VK_MBUTTON] = true;
			break;
		case WM_MBUTTONUP:
			g::pressed[VK_MBUTTON] = false;
			break;
		case WM_KEYDOWN:
		{
			if(HIWORD(lParam) & KF_REPEAT)
				g::held[wParam] = true;	
			else
				g::held[wParam] = false;

			g::pressed[wParam] = true;
			break;
		}
		case WM_KEYUP:
		{
			g::pressed[wParam] = false;
			break;
		}	
		case WM_XBUTTONDOWN:
		{
			switch(GET_XBUTTON_WPARAM(wParam))
			{
			case XBUTTON1:
				g::pressed[VK_XBUTTON1] = true;
			case XBUTTON2:
				g::pressed[VK_XBUTTON2] = true;
			default: break;
			}
		}
		case WM_XBUTTONUP:
		{
			switch(GET_XBUTTON_WPARAM(wParam))
			{
			case XBUTTON1:
				g::pressed[VK_XBUTTON1] = false;
			case XBUTTON2:
				g::pressed[VK_XBUTTON2] = false;
			default: break;
			}
		}
		default: break;
		}

		openMenu();

		if(g::imguiInit && option::get<bool>(menu_visible) && ImGui_ImplDX9_WndProcHandler(hWnd, uMsg, wParam, lParam))
			return true;

		return CallWindowProc(oWndProc, hWnd, uMsg, wParam, lParam);
	}
}

void openMenu()
{
	static auto held = false;
	static auto clicked = false;
	if(g::pressed[VK_HOME]) {
		clicked = false;
		held = true;
	}
	else if(!g::pressed[VK_HOME] && held) {
		clicked = true;
		held = false;
	}
	else {
		clicked = false;
		held = false;
	}

	if(clicked) {
		option::set(menu_visible, !option::get<bool>(menu_visible));
		auto mouseEnable = i::CVar->findVar("cl_mouseenable");
		mouseEnable->setValue(!option::get<bool>(menu_visible));
	}
}