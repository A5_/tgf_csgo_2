#include "Hooks.h"
#include "../HL/Material/IMaterial.h"

#include "../HL/Entities/C_CSPlayer.h"
#include "../HL/Entities/CbaseEntity.h"

#include "../Globals/Options.h"

void initKeyValues( KeyValues* kv_, std::string name_ )
{
	static auto address = u::mem::findPattern(
		m::getValue( MODULE_CLIENT ), S_ENC("55 8B EC 51 33 C0 C7 45") );

	using Fn = void(__thiscall*)( void* thisptr, const char* name );
	reinterpret_cast< Fn >( address )( kv_, name_.c_str() );
}

void loadFromBuffer(
	KeyValues* vk_, std::string name_, std::string buffer_ )
{
	static auto address = u::mem::findPattern(
		m::getValue( MODULE_CLIENT ),
		S_ENC("55 8B EC 83 E4 F8 83 EC 34 53 8B 5D 0C 89 4C 24 04") );

	using Fn = void(__thiscall*)(
		void* thisptr, const char* resourceName,
		const char* pBuffer, void* pFileSystem,
		const char* pPathID, void* pfnEvaluateSymbolProc );

	reinterpret_cast< Fn >( address )(
		vk_, name_.c_str(), buffer_.c_str(), nullptr, nullptr, nullptr );
}

IMaterial* createMaterial( bool ignorez, bool lit, bool wireframe )
{
	static auto created = 0;

	std::string type = lit ? S_ENC("VertexLitGeneric") : S_ENC("UnlitGeneric");

	auto matdata =
			S_ENC("\"") + type + S_ENC("\"\
		\n{\
		\n\t\"$basetexture\" \"vgui/white_additive\"\
		\n\t\"$envmap\" \"\"\
		\n\t\"$model\" \"1\"\
		\n\t\"$flat\" \"1\"\
		\n\t\"$nocull\" \"0\"\
		\n\t\"$selfillum\" \"1\"\
		\n\t\"$halflambert\" \"1\"\
		\n\t\"$nofog\" \"0\"\
		\n\t\"$ignorez\" \"") + std::to_string( ignorez ) + S_ENC("\"\
		\n\t\"$znearer\" \"0\"\
		\n\t\"$wireframe\" \"") + std::to_string( wireframe ) + S_ENC("\"\
        \n}\n");

	auto matname = S_ENC("custom_") + std::to_string( created );
	++created;

	auto keyValues = static_cast< KeyValues* >( malloc( sizeof(KeyValues) ) );

	initKeyValues( keyValues, type.c_str() );
	loadFromBuffer( keyValues, matname, matdata );

	auto material =
			i::MaterialSystem->createMaterial( matname.c_str(), keyValues );

	material->incrementReferenceCount();
	return material;
}

void forceMaterial( void* this_, IMaterial* material, hl::Color color )
{
	if( !material )
		return;
	i::RenderView->setBlend( 1.f );
	i::RenderView->setColorModulation( color.base() );
	i::ModelRender.forcedMaterialOverride( this_, material );
}

namespace hooks
{
	DrawModelExecuteFn oDrawModelExecute;

	enum eChamMode
	{
		flat,
		lit,
		wireframe,
	};

	void __fastcall DrawModelExecute(
		void* this_, int edx, void* ctx, void* state,
		const ModelRenderInfo_t& info, matrix3x4_t* bonetoworld )
	{
		static auto vis_flat = createMaterial( false, false, false );
		static auto vis_lit = createMaterial( false, true, false );
		static auto hid_flat = createMaterial( true, false, false );
		static auto hid_lit = createMaterial( true, true, false );
		static auto vis_wire = createMaterial( false, true, true );
		static auto hid_wire = createMaterial( true, true, true );

		IMaterial* visible;
		IMaterial* hidden;

		switch( option::get< eChamMode >( chams_mode ) )
		{
			case flat:
				visible = vis_flat;
				hidden = hid_flat;
				break;
			case lit:
				visible = vis_lit;
				hidden = hid_lit;
				break;
			case wireframe:
				visible = vis_wire;
				hidden = hid_wire;
				break;
			default:
				visible = vis_lit;
				hidden = hid_lit;
				break;
		}

		auto local = C_CSPlayer::getLocal()->getTeamNum();

		if( !info.pModel && !local && option::get< bool >( chams_toggle ) )
			oDrawModelExecute( this_, ctx, state, info, bonetoworld );

		std::string modelName = i::ModelInfo->GetModelName( info.pModel );

		auto entity =
				reinterpret_cast< CBaseEntity* >(
					i::ClientEntList->getClientEntity( info.entity_index ) );

		if( entity->compareClientID( CCSPlayer ) )
		{
			auto player = reinterpret_cast< C_CSPlayer* >( entity );

			if( player && player->isAlive() )
			{
				auto enemy = player->getTeamNum() != local;
				auto col_vis = enemy ?
					               hl::Color( 232, 209, 32 ) : hl::Color( 72, 219, 75 );
				auto col_hid = enemy ?
					               hl::Color( 200, 60, 60 ) : hl::Color( 84, 167, 255 );

				if( option::get< bool >( chams_xqz ) )
				{
					forceMaterial( this_, hidden, col_hid );
					oDrawModelExecute( this_, ctx, state, info, bonetoworld );
				}

				forceMaterial( this_, visible, col_vis );
			}
		}

		oDrawModelExecute( this_, ctx, state, info, bonetoworld );
		i::ModelRender.forcedMaterialOverride( this_, nullptr );
	}
}
