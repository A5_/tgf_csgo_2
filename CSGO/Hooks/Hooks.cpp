#include "Hooks.h"

Hooks& Hooks::get()
{
	static Hooks instance;
	return instance;
}

bool Hooks::startup()
{
	hooks::oWndProc = 
		reinterpret_cast<WNDPROC>(SetWindowLongPtr
		(
			g::window,
			GWL_WNDPROC,
			reinterpret_cast<LONG_PTR>(hooks::WndProc)
		));
	LOG("WndProc Hooked");

	if(!i::D3DDevice9.hookMethod(16, hooks::Reset, &hooks::oReset))
		return false;
	LOG("Reset Hooked");
	if(!i::D3DDevice9.hookMethod(42, hooks::EndScene, &hooks::oEndScene))
		return false;
	LOG("EndScene Hooked");
	if(!i::ClientMode.hookMethod(24, hooks::CreateMove, &hooks::oCreateMove))
		return false;
	LOG("CreateMove Hooked");
	if(!i::ModelRender.hookMethod(21, hooks::DrawModelExecute, &hooks::oDrawModelExecute))
		return false;
	LOG("DrawModelExecute Hooked");
	

	return true;
}

void Hooks::free()
{
	i::D3DDevice9.restoreTable();
	i::ClientMode.restoreTable();
	i::ModelRender.restoreTable();

	SetWindowLongPtr(
		g::window, GWL_WNDPROC, reinterpret_cast<LONG_PTR>(hooks::oWndProc));

}