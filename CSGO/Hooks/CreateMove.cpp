#include "Hooks.h"

namespace hooks
{
	CreateMoveFn oCreateMove;

	bool __stdcall CreateMove( float inputSampleTime, CUserCmd* cmd )
	{
		auto result = oCreateMove( inputSampleTime, cmd );

		return result;
	}
}
