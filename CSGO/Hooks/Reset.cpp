#include "Hooks.h"

#include "../ImGUI/imgui.h"
#include "../ImGUI/DX9/imgui_impl_dx9.h"
#include "../ImGUI/Draw.h"

namespace hooks
{
	ResetFn oReset;

	long __stdcall Reset(IDirect3DDevice9* pDevice, D3DPRESENT_PARAMETERS* pp)
	{
		if(!g::imguiInit) return oReset(pDevice, pp);

		ImGui_ImplDX9_InvalidateDeviceObjects();
		renderer->invalidateObjects();

		auto return_ = oReset(pDevice, pp);

		ImGui_ImplDX9_CreateDeviceObjects();
		renderer->createObjects();
		return return_;
	}
}