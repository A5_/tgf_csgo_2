#include "Hooks.h"

#include "../ImGUI/imgui.h"
#include "../ImGUI/DX9/imgui_impl_dx9.h"
#include "../ImGUI/Draw.h"

#include "../Menu/Menu.h"

#include "../Globals/Options.h"
#include "../Globals/Globals.h"

#include "../Cheats/ESP/ESP.h"

std::shared_ptr<Draw> renderer = nullptr;
std::unique_ptr<ESP> esp = nullptr;

void guiInit(IDirect3DDevice9* device);

namespace hooks
{
	EndSceneFn oEndScene;

	long __stdcall EndScene(IDirect3DDevice9* pDevice)
	{
		if(!g::imguiInit)
			guiInit(pDevice);

		gui::GetIO().MouseDrawCursor = option::get<bool>(menu_visible);

		ImGui_ImplDX9_NewFrame();

		renderer->beginRendering();
		renderer->addText({10, 10}, hl::Color::White(), drop_shadow, C_ENC("harmful.space"));
		if(i::Engine->isInGame())
		{
			esp->execute();
		}
		renderer->endRendering();

		if(option::get<bool>(menu_visible))
		{
			Menu::get().draw();
		}

		gui::Render();

		return oEndScene(pDevice);
	}
}

void guiInit(IDirect3DDevice9* device)
{
	//Initializes the GUI and the renderer
	ImGui_ImplDX9_Init(g::window, device);
	renderer = std::make_shared<Draw>(device);
	renderer->createObjects();
	esp = std::make_unique<ESP>(renderer);

	auto& style = gui::GetStyle();

	auto active = ImVec4(0.09f, 0.45f, 0.80f, 1.f);

	style.Colors[ImGuiCol_Text]				= ImVec4(0.78f, 0.78f, 0.78f, 1.00f);
	style.Colors[ImGuiCol_TextDisabled]		= ImVec4(0.59f, 0.59f, 0.59f, 1.00f);
	style.Colors[ImGuiCol_WindowBg]			= ImVec4(0.13f, 0.13f, 0.13f, 1.00f);
	style.Colors[ImGuiCol_ChildWindowBg]	= ImVec4(0.11f, 0.11f, 0.11f, 1.00f);
	style.Colors[ImGuiCol_Border]			= ImVec4(0.10f, 0.10f, 0.10f, 1.00f);
	style.Colors[ImGuiCol_FrameBg]			= ImVec4(0.80f, 0.80f, 0.80f, 0.09f);
	style.Colors[ImGuiCol_FrameBgHovered]	= ImVec4(0.39f, 0.39f, 0.39f, 1.00f);
	style.Colors[ImGuiCol_FrameBgActive]	= ImVec4(0.04f, 0.04f, 0.04f, 0.88f);
	style.Colors[ImGuiCol_TitleBg]			= ImVec4(0.00f, 0.00f, 0.00f, 1.00f);
	style.Colors[ImGuiCol_TitleBgCollapsed] = ImVec4(0.00f, 0.00f, 0.00f, 0.20f);
	style.Colors[ImGuiCol_TitleBgActive]	= ImVec4(0.00f, 0.00f, 0.00f, 1.00f);
	style.Colors[ImGuiCol_MenuBarBg]		= ImVec4(0.35f, 0.35f, 0.35f, 1.00f);
	style.Colors[ImGuiCol_ScrollbarBg]		= ImVec4(0.13f, 0.13f, 0.13f, 1.00f);
	style.Colors[ImGuiCol_ScrollbarGrab]	= active;
	style.Colors[ImGuiCol_ScrollbarGrabHovered] = active;
	style.Colors[ImGuiCol_ScrollbarGrabActive] = ImVec4(0.00f, 0.00f, 0.00f, 1.00f);
	style.Colors[ImGuiCol_SliderGrab]		= ImVec4(1.00f, 1.00f, 1.00f, 0.59f);
	style.Colors[ImGuiCol_SliderGrabActive] = active;
	style.Colors[ImGuiCol_Button]			= ImVec4(0.30f, 0.30f, 0.30f, 1.00f);
	style.Colors[ImGuiCol_ButtonHovered]	= ImVec4(0.39f, 0.39f, 0.39f, 1.00f);
	style.Colors[ImGuiCol_ButtonActive]		= active;
	style.Colors[ImGuiCol_Header]			= ImVec4(0.24f, 0.40f, 0.95f, 1.00f);
	style.Colors[ImGuiCol_HeaderHovered]	= active;
	style.Colors[ImGuiCol_HeaderActive]		= ImVec4(0.39f, 0.39f, 0.39f, 1.00f);
	style.Colors[ImGuiCol_ColumnHovered]	= ImVec4(0.70f, 0.02f, 0.60f, 0.22f);
	style.Colors[ImGuiCol_CloseButton]		= active;
	style.Colors[ImGuiCol_CloseButtonHovered] = active;
	style.Colors[ImGuiCol_Tab]				= ImVec4(0.13f, 0.13f, 0.13f, 1.00f);
	style.Colors[ImGuiCol_TabActive]		= active;
	style.Colors[ImGuiCol_TabHovered]		= ImVec4(0.30f, 0.30f, 0.30f, 1.00f);
	style.Colors[ImGuiCol_CheckMark]		= active;

	style.WindowRounding	= 0.f;
	style.ScrollbarSize		= 10.f;
	style.ScrollbarRounding = 0.f;
	style.GrabMinSize		= 5.f;

	g::imguiInit = true;
}