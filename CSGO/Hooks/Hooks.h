#include "../Include.h"
#include "../ImGUI/Draw.h"
#include "../HL/SDK.h"

class Hooks
{
public:
	static Hooks& get();

	static bool startup();
	static void free();
};

namespace hooks
{
	using EndSceneFn = long(__stdcall*)(IDirect3DDevice9* device);
	extern long __stdcall EndScene(IDirect3DDevice9* device);
	extern EndSceneFn oEndScene;

	using ResetFn = long(__stdcall*)(IDirect3DDevice9* device, D3DPRESENT_PARAMETERS* pp);
	extern long __stdcall Reset(IDirect3DDevice9* device, D3DPRESENT_PARAMETERS* pp);
	extern ResetFn oReset;

	using CreateMoveFn = bool(__stdcall*)(float, CUserCmd*);
	extern bool __stdcall CreateMove(float flInputSampleTime, CUserCmd* cmd);
	extern CreateMoveFn oCreateMove;

	using DrawModelExecuteFn = void(__thiscall*)(void*, void*, void*, const ModelRenderInfo_t&, matrix3x4_t*);
	extern void __fastcall DrawModelExecute(void* thisptr, int edx, void* ctx, void* state, const ModelRenderInfo_t &info, matrix3x4_t *bonetoworld);
	extern DrawModelExecuteFn oDrawModelExecute;

	using DrawModelFn = void(__stdcall*)(DrawModelResults_t*, const DrawModelInfo_t&, matrix3x4_t*, float*, float*, const Vector&, int);
	extern void __stdcall DrawModel(DrawModelResults_t *pResults, const DrawModelInfo_t& info, matrix3x4_t *pBoneToWorld, float *pFlexWeights, float *pFlexDelayedWeights, const Vector &modelOrigin, int flags);
	extern DrawModelFn oDrawModel;

	extern WNDPROC oWndProc;
	extern LRESULT __stdcall WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
}