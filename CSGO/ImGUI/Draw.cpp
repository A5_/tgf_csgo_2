#include "../StringEnc/cx_strenc.h"

#include "Draw.h"

#include "imgui.h"
#include "DX9/imgui_impl_dx9.h"

bool get_system_font_path(const std::string& name, std::string& path)
{
	char buffer[MAX_PATH];
	HKEY registryKey;

	GetWindowsDirectoryA(buffer, MAX_PATH);
	std::string fontsFolder = std::string(buffer) + S_ENC("\\Fonts\\");

	if(RegOpenKeyExA(HKEY_LOCAL_MACHINE,
		C_ENC("Software\\Microsoft\\Windows NT\\CurrentVersion\\Fonts"),
		0, KEY_READ, &registryKey)) 
	{
		return false;
	}

	uint32_t valueIndex = 0;
	char valueName[MAX_PATH];
	uint8_t valueData[MAX_PATH];
	std::wstring wsFontFile;

	do 
	{
		uint32_t valueNameSize = MAX_PATH;
		uint32_t valueDataSize = MAX_PATH;
		uint32_t valueType;

		auto error = RegEnumValueA(
			registryKey,
			valueIndex,
			valueName,
			reinterpret_cast<DWORD*>(&valueNameSize),
			nullptr,
			reinterpret_cast<DWORD*>(&valueType),
			valueData,
			reinterpret_cast<DWORD*>(&valueDataSize));

		valueIndex++;

		if(error == ERROR_NO_MORE_ITEMS) 
		{
			RegCloseKey(registryKey);
			return false;
		}

		if(error || valueType != REG_SZ)
		{
			continue;
		}

		if(_strnicmp(name.data(), valueName, name.size()) == 0)
		{
			path = fontsFolder +
				std::string(reinterpret_cast<char*>(valueData), valueDataSize);

			RegCloseKey(registryKey);
			return true;
		}
	} while(true);
}

Draw::Draw(IDirect3DDevice9* device)
{
	_device = device;
	_texture = nullptr;
	_drawList = nullptr;
}

Draw::~Draw()
{

}

void Draw::createObjects()
{
	_drawList = new ImDrawList();

	auto font_path = std::string{};

	uint8_t* pixel_data;

	int width,
		height,
		bytes_per_pixel;

	if(!get_system_font_path(FontName, font_path)) return;

	_fonts.AddFontFromFileTTF(font_path.data(),
		FontSize, nullptr, _fonts.GetGlyphRangesDefault());

	_fonts.GetTexDataAsRGBA32(&pixel_data, &width, &height, &bytes_per_pixel);

	auto hr = _device->CreateTexture(
		width, height,
		1,
		D3DUSAGE_DYNAMIC,
		D3DFMT_A8R8G8B8,
		D3DPOOL_DEFAULT,
		&_texture,
		nullptr);

	if(FAILED(hr)) return;

	D3DLOCKED_RECT tex_locked_rect;
	if(_texture->LockRect(0, &tex_locked_rect, nullptr, 0) != D3D_OK)
		return;
	for(int y = 0; y < height; y++)
		memcpy(static_cast<uint8_t*>(tex_locked_rect.pBits)
			+ tex_locked_rect.Pitch * y, pixel_data + 
			(width * bytes_per_pixel) * y, (width * bytes_per_pixel));
	_texture->UnlockRect(0);

	_fonts.TexID = _texture;
}

void Draw::invalidateObjects()
{
	if(_texture) _texture->Release();
	_texture = nullptr;

	_fonts.Clear();

	if(_drawList)
		delete _drawList;
	_drawList = nullptr;
}

void Draw::beginRendering()
{
	_drawData.Valid = false;

	_drawList->Clear();
	_drawList->PushClipRectFullScreen();
}

void Draw::endRendering()
{
	ImGui_ImplDX9_RenderDrawLists(getDrawData());
}

void Draw::addText(
	ImVec2 point, hl::Color color,
	text_flags flags, const char* format, ...)
{
	static const auto MAX_BUFFER_SIZE = 1024;
	static char buffer[MAX_BUFFER_SIZE] = "";

	auto font = _fonts.Fonts[0];

	_drawList->PushTextureID(_fonts.TexID);

	va_list va;
	va_start(va, format);
	vsnprintf_s(buffer, MAX_BUFFER_SIZE, format, va);
	va_end(va);

	if(flags & centered_x || flags & centered_y) 
	{
		auto text_size = font->CalcTextSizeA(font->FontSize, FLT_MAX, 0.0f, buffer);
		if(flags & centered_x)
			point.x -= text_size.x / 2;
		if(flags & centered_y)
			point.y -= text_size.y / 2;
	}

	if(flags & outline)
	{
		_drawList->AddText(
			font, font->FontSize, ImVec2{point.x - 1, point.y - 1}, 0xFF000000, buffer);
		_drawList->AddText(
			font, font->FontSize, ImVec2{point.x + 1, point.y}, 0xFF000000, buffer);
		_drawList->AddText(
			font, font->FontSize, ImVec2{point.x    , point.y + 1}, 0xFF000000, buffer);
		_drawList->AddText(
			font, font->FontSize, ImVec2{point.x - 1, point.y}, 0xFF000000, buffer);
	}

	if(flags & drop_shadow && !(flags & outline))
		_drawList->AddText(
			font, font->FontSize, ImVec2{point.x + 1, point.y + 1}, 0xFF000000, buffer);

	_drawList->AddText(font, font->FontSize, point, color.getRGBA(), buffer);
	_drawList->PopTextureID();
}

void Draw::addRect(
	const ImVec2& a, float w,
	float h, hl::Color color,
	float rounding,
	int rounding_corners_flags,
	float thickness) const
{
	_drawList->AddRect(a, 
	{a.x + w, a.y + h}, 
		color.getRGBA(), rounding, 
		rounding_corners_flags, thickness);
}

void Draw::addLine(
	const ImVec2& a, const ImVec2& b,
	hl::Color color, float thickness) const
{
	_drawList->AddLine(a, b, color.getRGBA(), thickness);
}

void Draw::addRect(
	const ImVec2& a, const ImVec2& b,
	hl::Color color, float rounding,
	int rounding_corners_flags , 
	float thickness /*= 1.0f*/) const
{
	_drawList->AddRect(a, b, color.getRGBA(), 
		rounding, rounding_corners_flags, thickness);
}

void Draw::addRectFilled(
	const ImVec2& a, const ImVec2& b,
	hl::Color color, float rounding, 
	int rounding_corners_flags) const
{
	_drawList->AddRectFilled(a, b, color.getRGBA(),
		rounding, rounding_corners_flags);
}

void Draw::addRectFilledMultiColor(
	const ImVec2& a, const ImVec2& b, 
	hl::Color color_upr_left,
	hl::Color color_upr_right,
	hl::Color color_bot_right,
	hl::Color color_bot_left) const
{
	_drawList->AddRectFilledMultiColor(
		a, b,
		color_upr_left.getRGBA(),
		color_upr_right.getRGBA(),
		color_bot_right.getRGBA(),
		color_bot_left.getRGBA()
	);
}

void Draw::addQuad(
	const ImVec2& a, const ImVec2& b,
	const ImVec2& c, const ImVec2& d,
	hl::Color color,float thickness ) const
{
	_drawList->AddQuad(a, b, c, d, 
		color.getRGBA(), thickness);
}

void Draw::addQuadFilled(
	const ImVec2& a, const ImVec2& b,
	const ImVec2& c, const ImVec2& d,
	hl::Color color) const
{
	_drawList->AddQuadFilled(a, b, c, d, color.getRGBA());
}

void Draw::addTriangle(
	const ImVec2& a, const ImVec2& b, 
	const ImVec2& c, hl::Color color, 
	float thickness /*= 1.0f*/) const
{
	_drawList->AddTriangle(a, b, c, color.getRGBA(), thickness);
}

void Draw::addTriangleFilled(
	const ImVec2& a, const ImVec2& b,
	const ImVec2& c, hl::Color color) const
{
	_drawList->AddTriangleFilled(a, b, c, color.getRGBA());
}

void Draw::addCircle(
	const ImVec2& centre, float radius,
	hl::Color color, int num_segments /*= 12*/,
	float thickness /*= 1.0f*/) const
{
	_drawList->AddCircle(centre, radius, color.getRGBA(), num_segments, thickness);
}

void Draw::addCircleFilled(
	const ImVec2& centre, float radius,
	hl::Color color, int num_segments /*= 12*/) const
{
	_drawList->AddCircleFilled(centre, radius, color.getRGBA(), num_segments);
}

void Draw::addPolyline(
	const ImVec2* points, const int num_points,
	hl::Color color, bool closed, 
	float thickness, bool anti_aliased) const
{
	_drawList->AddPolyline(points, num_points, color.getRGBA(), closed, thickness, anti_aliased);
}

void Draw::addConvexPolyFilled(
	const ImVec2* points, const int num_points,
	hl::Color color, bool anti_aliased) const
{
	_drawList->AddConvexPolyFilled(points, num_points, color.getRGBA(), anti_aliased);
}

void Draw::addBezierCurve(
	const ImVec2& pos0, const ImVec2& cp0,
	const ImVec2& cp1, const ImVec2& pos1,
	hl::Color color, float thickness, 
	int num_segments /*= 0*/) const
{
	_drawList->AddBezierCurve(pos0, cp0, cp1, pos1, color.getRGBA(), thickness, num_segments);
}

ImDrawData* Draw::getDrawData()
{
	if(!_drawList->VtxBuffer.empty())
	{
		_drawData.Valid = true;
		_drawData.CmdLists = &_drawList;
		_drawData.CmdListsCount = 1;
		_drawData.TotalVtxCount = _drawList->VtxBuffer.Size;
		_drawData.TotalIdxCount = _drawList->IdxBuffer.Size;
	}
	return &_drawData;
}