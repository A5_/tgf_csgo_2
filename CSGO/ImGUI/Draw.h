#pragma once

#include <memory>
#include <string>
#include <vector>
#include <windows.h>

#include <d3d9.h>
#include "imgui.h"
#include "imgui_internal.h"

#include "../HL/Utl/Color.h"

#define RGBA(r, g, b, a) ((a << 24) | (b << 16) | (g << 8) | r)

enum text_flags
{
	centered_x = 1,
	centered_y = 2,
	outline = 4,
	drop_shadow = 8
};

DEFINE_ENUM_FLAG_OPERATORS(text_flags)

class Draw
{
public:
	Draw(IDirect3DDevice9* device);
	~Draw();

	void createObjects();
	void invalidateObjects();
	void beginRendering();
	void endRendering();

	void addText(ImVec2 point, hl::Color color, text_flags flags, const char* format, ...);
	void addRect(const ImVec2& a, float w, float h, hl::Color col, float rounding = 0.0f, int rounding_corners_flags = ~0, float thickness = 1.0f) const;

	void addLine(const ImVec2& a, const ImVec2& b, hl::Color col, float thickness = 1.0f) const;
	void addRect(const ImVec2& a, const ImVec2& b, hl::Color col, float rounding = 0.0f, int rounding_corners_flags = ~0, float thickness = 1.0f) const;
	void addRectFilled(const ImVec2& a, const ImVec2& b, hl::Color col, float rounding = 0.0f, int rounding_corners_flags = ~0) const;
	void addRectFilledMultiColor(const ImVec2& a, const ImVec2& b, hl::Color col_upr_left, hl::Color col_upr_right, hl::Color col_bot_right, hl::Color col_bot_left) const;
	void addQuad(const ImVec2& a, const ImVec2& b, const ImVec2& c, const ImVec2& d, hl::Color col, float thickness = 1.0f) const;
	void addQuadFilled(const ImVec2& a, const ImVec2& b, const ImVec2& c, const ImVec2& d, hl::Color col) const;
	void addTriangle(const ImVec2& a, const ImVec2& b, const ImVec2& c, hl::Color col, float thickness = 1.0f) const;
	void addTriangleFilled(const ImVec2& a, const ImVec2& b, const ImVec2& c, hl::Color col) const;
	void addCircle(const ImVec2& centre, float radius, hl::Color col, int num_segments = 12, float thickness = 1.0f) const;
	void addCircleFilled(const ImVec2& centre, float radius, hl::Color col, int num_segments = 12) const;
	void addPolyline(const ImVec2* points, const int num_points, hl::Color col, bool closed, float thickness, bool anti_aliased) const;
	void addConvexPolyFilled(const ImVec2* points, const int num_points, hl::Color col, bool anti_aliased) const;
	void addBezierCurve(const ImVec2& pos0, const ImVec2& cp0, const ImVec2& cp1, const ImVec2& pos1, hl::Color col, float thickness, int num_segments = 0) const;

private:
	ImDrawData* getDrawData();

	IDirect3DDevice9*   _device;
	IDirect3DTexture9*  _texture;
	ImDrawList*         _drawList;
	ImDrawData          _drawData;
	ImFontAtlas         _fonts;

	const std::string   FontName = "Consolas";
	const float         FontSize = 12.0f;
};

extern std::shared_ptr<Draw> renderer;