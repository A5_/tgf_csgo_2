#pragma once
#include "imgui.h"

class ImGuiTab
{
public:
	void Draw( int* selected )
	{
		auto& style = gui::GetStyle();
		auto color = style.Colors[ ImGuiCol_Tab ];
		auto colorActive = style.Colors[ ImGuiCol_TabActive ];
		auto colorHover = style.Colors[ ImGuiCol_TabHovered ];
		auto max = gui::GetContentRegionMax();
		auto size_x = max.x / labels_.size() - 6.f;
		auto window = gui::GetCurrentWindow();

		auto pos = window->DC.CursorPos;
		const ImRect bb( pos, { pos.x + max.x - 9.f, pos.y + 20.f } );
		window->DrawList->AddRectFilled( bb.Min, bb.Max, gui::GetColorU32( { 0.f, 0.f, 0.f, 0.5f } ), 0 );

		for( auto i = 0; i < labels_.size(); i++ )
		{
			// push the style
			if( i == *selected )
			{
				style.Colors[ ImGuiCol_Tab ] = colorActive;
				style.Colors[ ImGuiCol_TabActive ] = colorActive;
				style.Colors[ ImGuiCol_TabHovered ] = colorActive;
			} else
			{
				style.Colors[ ImGuiCol_Tab ] = color;
				style.Colors[ ImGuiCol_TabActive ] = colorActive;
				style.Colors[ ImGuiCol_TabHovered ] = colorHover;
			}

			// Draw the button
			if( gui::ButtonTab( labels_[ i ].c_str(), { size_x, 0 } ) )
				*selected = i;

			if( i != labels_.size() - 1 )
				gui::SameLine();
		}

		gui::Spacing();

		// Restore the style
		style.Colors[ ImGuiCol_Tab ] = color;
		style.Colors[ ImGuiCol_TabActive ] = colorActive;
		style.Colors[ ImGuiCol_TabHovered ] = colorHover;
	}

	void newTab( std::string s ) { labels_.push_back( s ); }
private:
	std::vector< std::string > labels_;
};

namespace gui
{
	inline void ColumnLine()
	{
		auto draw_list = GetWindowDrawList();
		auto p = GetCursorScreenPos();
		draw_list->AddLine( ImVec2( p.x - 9999, p.y ), ImVec2( p.x + 9999, p.y ), GetColorU32( ImGuiCol_Border ) );
	}
}
